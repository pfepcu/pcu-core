package ca.pcuets.core.fixture;

import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionEntity;

public class SessionCourseEntityFixture {

    public static SessionCourseEntity get(CourseEntity courseEntity, SessionEntity sessionEntity) {

        SessionCourseEntity sessionCourseEntity = new SessionCourseEntity();
        sessionCourseEntity.setCourse(courseEntity);
        sessionCourseEntity.setSession(sessionEntity);

        return sessionCourseEntity;
    }
}
