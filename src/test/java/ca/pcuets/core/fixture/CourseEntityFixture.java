package ca.pcuets.core.fixture;

import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.CoursePrerequisiteEntity;
import java.util.List;

public class CourseEntityFixture {

    public static CourseEntity get(String acronym, String name, String description, Integer credits) {

        CourseEntity expectedCourseEntity = new CourseEntity();
        expectedCourseEntity.setName(name);
        expectedCourseEntity.setAcronym(acronym);
        expectedCourseEntity.setDescription(description);
        expectedCourseEntity.setCredits(credits);
        return expectedCourseEntity;
    }

    public static CourseEntity get(Integer id, String acronym, String name, String description, Integer credits, Integer requiresCredits,
                                   CourseEntity successor, List<CoursePrerequisiteEntity> prerequisites){
        CourseEntity courseEntity = new CourseEntity();
        courseEntity.setId(id);
        courseEntity.setDescription(description);
        courseEntity.setName(name);
        courseEntity.setAcronym(acronym);
        courseEntity.setCredits(credits);
        courseEntity.setRequiredCredits(requiresCredits);
        courseEntity.setSuccessor(successor);
        courseEntity.setPrerequisites(prerequisites);

        return courseEntity;
    }
}
