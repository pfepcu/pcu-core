package ca.pcuets.core.fixture;

import ca.pcuets.core.dto.User;

public class UserFixture {

    public static User get(String firstName, String lastName, String email, String password) {

        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        return user;
    }
}
