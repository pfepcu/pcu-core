package ca.pcuets.core.fixture;

import ca.pcuets.core.etl.transformer.payload.SessionScheduleTransformerPayload;

public class SessionScheduleTransformerPayloadFixture {

    public static SessionScheduleTransformerPayload get(String session, Boolean isDay, Boolean isEvening) {

        SessionScheduleTransformerPayload sessionScheduleTransformerPayload = new SessionScheduleTransformerPayload();
        sessionScheduleTransformerPayload.setSession(session);
        sessionScheduleTransformerPayload.setDay(isDay);
        sessionScheduleTransformerPayload.setEvening(isEvening);

        return sessionScheduleTransformerPayload;
    }
}
