package ca.pcuets.core.fixture;

import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;

public class SessionEntityFixture {

    public static SessionEntity get(SeasonEnum season, Integer year) {

        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setSeason(season);
        sessionEntity.setYear(year);

        return sessionEntity;
    }
}
