package ca.pcuets.core.fixture;

import ca.pcuets.core.model.UserEntity;

public class UserEntityFixture {

    public static UserEntity get(Integer id, String email, String password) {

        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        userEntity.setEmail(email);
        userEntity.setPassword(password);
        return userEntity;
    }
}
