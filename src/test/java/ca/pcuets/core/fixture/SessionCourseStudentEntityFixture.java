package ca.pcuets.core.fixture;

import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.UserEntity;

public class SessionCourseStudentEntityFixture {

    public static SessionCourseStudentEntity get(SessionCourseEntity sessionCourseEntity) {

        SessionCourseStudentEntity sessionCourseStudentEntity = new SessionCourseStudentEntity();
        sessionCourseStudentEntity.setSessionCourse(sessionCourseEntity);
        sessionCourseStudentEntity.setUser(new UserEntity());
        return sessionCourseStudentEntity;
    }

}
