package ca.pcuets.core.fixture;

import ca.pcuets.core.etl.transformer.payload.CourseTransformerPayload;

public class CourseTransformerPayloadFixture {

    public static CourseTransformerPayload get(String acronym, String name, String description, Integer credits) {

        CourseTransformerPayload courseTransformerPayload = new CourseTransformerPayload();
        courseTransformerPayload.setName(name);
        courseTransformerPayload.setAcronym(acronym);
        courseTransformerPayload.setDescription(description);
        courseTransformerPayload.setCredits(credits);

        return courseTransformerPayload;
    }
}
