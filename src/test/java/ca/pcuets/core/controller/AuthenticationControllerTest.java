package ca.pcuets.core.controller;

import ca.pcuets.core.dao.UserDAO;
import ca.pcuets.core.dto.Credential;
import ca.pcuets.core.fixture.UserEntityFixture;
import ca.pcuets.core.model.UserEntity;
import ca.pcuets.core.helper.SessionHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationControllerTest {

    @InjectMocks
    AuthenticationController authenticationService;

    @Mock
    UserDAO userDAO;

    @Mock
    SessionHelper sessionHelper;

    @Test
    public void login() throws Exception {

        Credential credential = new Credential();
        credential.setEmail("test@test.test");
        credential.setPassword("testasdasd");

        Optional<UserEntity> expectedUserEntityOptional =
                Optional.of(UserEntityFixture.get(1, "test@test.test", "$2a$05$D/TmnkcCqubXyV5eJg5rIeIXMF1iNyF8kv3N64B.lfZpW19KaMnUS"));

        when(userDAO.findByEmail("test@test.test"))
                .thenReturn(expectedUserEntityOptional);

        authenticationService.login(credential);

        verify(sessionHelper).createSession(credential, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void login_wrongPassword() throws Exception {

        Credential credential = new Credential();
        credential.setEmail("test@test.test");
        credential.setPassword("testadasd");

        Optional<UserEntity> expectedUserEntityOptional =
                Optional.of(UserEntityFixture.get(1, "test@test.test", "$2a$05$D/TmnkcCqubXyV5eJg5rIeIXMF1iNyF8kv3N64B.lfZpW19KaMnUS"));

        when(userDAO.findByEmail("test@test.test"))
                .thenReturn(expectedUserEntityOptional);

        authenticationService.login(credential);

        verify(sessionHelper, never()).createSession(credential, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void login_noUserFound() throws Exception {

        Credential credential = new Credential();
        credential.setEmail("test@test.test");
        credential.setPassword("testadasd");

        Optional<UserEntity> expectedUserEntityOptional = Optional.empty();

        when(userDAO.findByEmail("test@test.test"))
                .thenReturn(expectedUserEntityOptional);

        authenticationService.login(credential);

        verify(sessionHelper, never()).createSession(credential, 1);
    }

}