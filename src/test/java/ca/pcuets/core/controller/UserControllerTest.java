package ca.pcuets.core.controller;

import ca.pcuets.core.dao.PreferenceDAO;
import ca.pcuets.core.dao.UserDAO;
import ca.pcuets.core.dto.User;
import ca.pcuets.core.factory.PreferenceEntityFactory;
import ca.pcuets.core.fixture.UserEntityFixture;
import ca.pcuets.core.fixture.UserFixture;
import ca.pcuets.core.helper.SessionHelper;
import ca.pcuets.core.mapper.UserEntityMapper;
import ca.pcuets.core.model.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @InjectMocks
    UserController userService;

    @Mock
    UserDAO userDAO;

    @Mock
    UserEntityMapper userEntityMapper;

    @Mock
    PreferenceEntityFactory preferenceEntityFactory;

    @Mock
    PreferenceDAO preferenceDAO;

    @Mock
    SessionHelper sessionHelper;

    @Test
    public void create() throws Exception {

        User user = UserFixture.get("firstname", "lastname", "email@email.email", "password");
        Optional<UserEntity> emailValidationUserEntityOptional = Optional.empty();
        UserEntity expectedUserEntity = UserEntityFixture.get(null, "email@email.email", "hashedpw");

        when(userDAO.findByEmail("email@email.email"))
                .thenReturn(emailValidationUserEntityOptional);

        when(userEntityMapper.map(user))
                .thenReturn(expectedUserEntity);

        when(userDAO.save(expectedUserEntity))
                .thenReturn(expectedUserEntity);

        userService.create(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_emailAlreadyAssociated() throws Exception {

        User user = UserFixture.get("firstname", "lastname", "email@email.email", "password");
        UserEntity existingUserEntity = UserEntityFixture.get(null, "email@email.email", "hashedpw");
        Optional<UserEntity> emailValidationUserEntityOptional = Optional.of(existingUserEntity);

        when(userDAO.findByEmail("email@email.email"))
                .thenReturn(emailValidationUserEntityOptional);

        userService.create(user);
    }

    @Test
    public void update() throws Exception {

        User user = UserFixture.get("firstname", "lastname", "email@email.email", "password");
        UserEntity existingUserEntity = UserEntityFixture.get(null, "email@email.email2", "hashedpw2");
        Optional<UserEntity> existingUserEntityOptional = Optional.of(existingUserEntity);
        UserEntity expectedUserEntity = UserEntityFixture.get(null, "email@email.email2", "hashedpw2");

        when(userDAO.findById(1))
                .thenReturn(existingUserEntityOptional);

        when(userEntityMapper.map(existingUserEntity, user))
                .thenReturn(expectedUserEntity);

        userService.update(1, user);

        verify(userDAO).save(expectedUserEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_inexistantUser() throws Exception {

        User user = UserFixture.get("firstname", "lastname", "email@email.email", "password");
        Optional<UserEntity> existingUserEntityOptional = Optional.empty();

        when(userDAO.findById(1))
                .thenReturn(existingUserEntityOptional);

        userService.update(1, user);

        verify(userDAO, never()).save(any(UserEntity.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_updateOtherUser() throws Exception {

        User user = UserFixture.get("firstname", "lastname", "email@email.email", "password");

        doThrow(IllegalArgumentException.class)
                .when(sessionHelper).validateSessionUserId(1);

        userService.update(1, user);

    }
}