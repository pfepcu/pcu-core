package ca.pcuets.core.etl.loader;

import ca.pcuets.core.dao.CourseDAO;
import ca.pcuets.core.fixture.CourseEntityFixture;
import ca.pcuets.core.fixture.CourseTransformerPayloadFixture;
import ca.pcuets.core.etl.transformer.payload.CourseTransformerPayload;
import ca.pcuets.core.model.CourseEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CourseLoaderTest {

    @InjectMocks
    private CourseLoader courseLoader;

    @Mock
    private CourseDAO courseDAO;

    @Captor
    private ArgumentCaptor<CourseEntity> courseEntityArgumentCaptor;

    @Test
    public void load_createCourseEntity() throws Exception {

        CourseTransformerPayload courseTransformerPayload =
                CourseTransformerPayloadFixture.get("ACR-102", "Cours 1", "This is a description", 12);

        CourseEntity expectedCourseEntity =
                CourseEntityFixture.get("ACR-102", "Cours 1", "This is a description", 12);

        courseLoader.load(courseTransformerPayload);

        verify(courseDAO).save(courseEntityArgumentCaptor.capture());

        assertThat(courseEntityArgumentCaptor.getValue(), is(equalTo(expectedCourseEntity)));
    }

    @Test
    public void load_createCourseEntity_nullDescription() throws Exception {

        CourseTransformerPayload courseTransformerPayload =
                CourseTransformerPayloadFixture.get("ACR-102", "Cours 1", null, 12);

        CourseEntity expectedCourseEntity =
                CourseEntityFixture.get("ACR-102", "Cours 1", null, 12);

        courseLoader.load(courseTransformerPayload);

        verify(courseDAO).save(courseEntityArgumentCaptor.capture());

        assertThat(courseEntityArgumentCaptor.getValue(), is(equalTo(expectedCourseEntity)));
    }

    @Test
    public void load_updateCourseEntity() throws Exception {

        CourseTransformerPayload courseTransformerPayload =
                CourseTransformerPayloadFixture.get("ACR-102", "Cours 1", "New description", 3);

        CourseEntity foundCourseEntity =
                CourseEntityFixture.get("ACR-102", "Cours 1", "Previous description", 4);

        when(courseDAO.findByAcronym("ACR-102"))
                .thenReturn(foundCourseEntity);

        courseLoader.load(courseTransformerPayload);

        verify(courseDAO).save(courseEntityArgumentCaptor.capture());

        CourseEntity capturedCourse = courseEntityArgumentCaptor.getValue();
        assertThat(capturedCourse.getDescription(), is(equalTo("New description")));
        assertThat(capturedCourse.getCredits(), is(equalTo(3)));
    }

    @Test
    public void load_updateCourseEntity_nullPayloadDescription() throws Exception {

        CourseTransformerPayload courseTransformerPayload =
                CourseTransformerPayloadFixture.get("ACR-102", "Cours 1", null, 3);

        CourseEntity foundCourseEntity =
                CourseEntityFixture.get("ACR-102", "Cours 1", "Previous description", 4);

        when(courseDAO.findByAcronym("ACR-102"))
                .thenReturn(foundCourseEntity);

        courseLoader.load(courseTransformerPayload);

        verify(courseDAO).save(courseEntityArgumentCaptor.capture());

        CourseEntity capturedCourse = courseEntityArgumentCaptor.getValue();
        assertThat(capturedCourse.getDescription(), is(equalTo("Previous description")));
        assertThat(capturedCourse.getCredits(), is(equalTo(3)));
    }

    @Test
    public void load_updateCourseEntity_nullEntityDescription() throws Exception {

        CourseTransformerPayload courseTransformerPayload =
                CourseTransformerPayloadFixture.get("ACR-102", "Cours 1", "New description", 3);

        CourseEntity foundCourseEntity =
                CourseEntityFixture.get("ACR-102", "Cours 1", null, 4);

        when(courseDAO.findByAcronym("ACR-102"))
                .thenReturn(foundCourseEntity);

        courseLoader.load(courseTransformerPayload);

        verify(courseDAO).save(courseEntityArgumentCaptor.capture());

        CourseEntity capturedCourse = courseEntityArgumentCaptor.getValue();
        assertThat(capturedCourse.getDescription(), is(equalTo("New description")));
        assertThat(capturedCourse.getCredits(), is(equalTo(3)));
    }
}