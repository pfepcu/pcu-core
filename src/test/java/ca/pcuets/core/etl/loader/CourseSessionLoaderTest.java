package ca.pcuets.core.etl.loader;

import ca.pcuets.core.dao.CourseDAO;
import ca.pcuets.core.dao.SessionCourseDAO;
import ca.pcuets.core.dao.SessionDAO;
import ca.pcuets.core.fixture.CourseEntityFixture;
import ca.pcuets.core.fixture.CourseTransformerPayloadFixture;
import ca.pcuets.core.fixture.SessionCourseEntityFixture;
import ca.pcuets.core.fixture.SessionEntityFixture;
import ca.pcuets.core.fixture.SessionScheduleTransformerPayloadFixture;
import ca.pcuets.core.etl.transformer.payload.CourseSessionTransformerPayload;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CourseSessionLoaderTest {

    @InjectMocks
    private CourseSessionLoader courseSessionLoader;

    @Mock
    private CourseLoader courseLoader;

    @Mock
    private CourseDAO courseDAO;

    @Mock
    private SessionDAO sessionDAO;

    @Mock
    private SessionCourseDAO sessionCourseDAO;

    @Captor
    private ArgumentCaptor<SessionCourseEntity> sessionCourseEntityArgumentCaptor;

    @Test
    public void load() throws Exception {

        CourseSessionTransformerPayload courseSessionTransformerPayload = new CourseSessionTransformerPayload();
        courseSessionTransformerPayload.setCourseTransformerPayload(
                CourseTransformerPayloadFixture.get("ITT-123", "Title", "Description", 12)
        );
        courseSessionTransformerPayload.setSessionScheduleTransformerPayloads(Arrays.asList(
                SessionScheduleTransformerPayloadFixture.get("A16", true, false),
                SessionScheduleTransformerPayloadFixture.get("H16", false, true)
        ));

        CourseEntity expectedCourseEntity =
                CourseEntityFixture.get("ITT-123", "Title", "Description", 12);

        SessionEntity expectedSessionEntityA16 =
                SessionEntityFixture.get(SeasonEnum.AUTUMN, 2016);
        SessionCourseEntity expectedSessionCourseEntityA16 =
                SessionCourseEntityFixture.get(expectedCourseEntity, expectedSessionEntityA16);
        when(courseDAO.findByAcronym("ITT-123"))
                .thenReturn(expectedCourseEntity);
        when(sessionDAO.findBySeasonAndYear(SeasonEnum.AUTUMN, 2016))
                .thenReturn(expectedSessionEntityA16);
        when(sessionCourseDAO.findByCourseAndSession(expectedCourseEntity, expectedSessionEntityA16))
                .thenReturn(expectedSessionCourseEntityA16);

        SessionEntity expectedSessionEntityH16 =
                SessionEntityFixture.get(SeasonEnum.WINTER, 2016);
        SessionCourseEntity expectedSessionCourseEntityH16 =
                SessionCourseEntityFixture.get(expectedCourseEntity, expectedSessionEntityH16);
        when(courseDAO.findByAcronym("ITT-123"))
                .thenReturn(expectedCourseEntity);
        when(sessionDAO.findBySeasonAndYear(SeasonEnum.WINTER, 2016))
                .thenReturn(expectedSessionEntityH16);
        when(sessionCourseDAO.findByCourseAndSession(expectedCourseEntity, expectedSessionEntityH16))
                .thenReturn(expectedSessionCourseEntityH16);

        courseSessionLoader.load(courseSessionTransformerPayload);

        verify(courseLoader, times(1))
                .load(courseSessionTransformerPayload.getCourseTransformerPayload());
        verify(sessionCourseDAO, times(2))
                .save(sessionCourseEntityArgumentCaptor.capture());

        assertThat(sessionCourseEntityArgumentCaptor.getAllValues().get(0).getDay(), is(equalTo(true)));
        assertThat(sessionCourseEntityArgumentCaptor.getAllValues().get(0).getEvening(), is(equalTo(false)));
        assertThat(sessionCourseEntityArgumentCaptor.getAllValues().get(1).getDay(), is(equalTo(false)));
        assertThat(sessionCourseEntityArgumentCaptor.getAllValues().get(1).getEvening(), is(equalTo(true)));
    }

    @Test
    public void load_noSessionSchedule() throws Exception {

        CourseSessionTransformerPayload courseSessionTransformerPayload = new CourseSessionTransformerPayload();
        courseSessionTransformerPayload.setCourseTransformerPayload(
                CourseTransformerPayloadFixture.get("ITT-123", "Title", "Description", 12)
        );
        courseSessionTransformerPayload.setSessionScheduleTransformerPayloads(Collections.emptyList());

        courseSessionLoader.load(courseSessionTransformerPayload);

        verify(courseLoader, times(0))
                .load(courseSessionTransformerPayload.getCourseTransformerPayload());
    }
}