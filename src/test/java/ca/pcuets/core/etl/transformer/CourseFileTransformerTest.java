package ca.pcuets.core.etl.transformer;

import ca.pcuets.core.etl.extractor.payload.HtmlExtractorPayload;
import ca.pcuets.core.etl.transformer.payload.CourseTransformerPayload;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class CourseFileTransformerTest {

    @InjectMocks
    CourseFileTransformer courseFileTransformer;

    @Test
    public void transform() throws Exception {

        Document document = Jsoup.parse(
            "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneCenter_FicheCours_LabelDescription\">" +
                "Description" +
            "</div>" +
            "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_LabelCredits\">" +
                "Crédits : 3 cr." +
            "</div>" +
            "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_LabelPrealables\">" +
                "<a>AAA200</a>" +
                "<a>BBB300</a>" +
            "</div>" +
            "<a id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_HyperLinkPlanCours\" " +
                "href=\"https://planets.etsmtl.ca/public/Versionpdf.aspx?sigle=LLL111\">" +
                "Plan de cours" +
            "</a>");

        HtmlExtractorPayload htmlExtractorPayload = new HtmlExtractorPayload();
        htmlExtractorPayload.setDocument(document);

        CourseTransformerPayload courseTransformerPayload =
                courseFileTransformer.transform(htmlExtractorPayload);

        assertThat(courseTransformerPayload.getAcronym(), is(equalTo("LLL111")));
        assertThat(courseTransformerPayload.getDescription(), is(equalTo("Description")));
        assertThat(courseTransformerPayload.getCredits(), is(equalTo(3)));
        assertThat(courseTransformerPayload.getPrerequisites(), is(equalTo(Arrays.asList("AAA200", "BBB300"))));
    }

    @Test
    public void transform_noCredits() throws Exception {

        Document document = Jsoup.parse(
            "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneCenter_FicheCours_LabelDescription\">" +
                "Description" +
            "</div>" +
            "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_LabelCredits\">" +
            "</div>" +
            "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_LabelPrealables\">" +
                "<a>AAA200</a>" +
                "<a>BBB300</a>" +
            "</div>" +
            "<a id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_HyperLinkPlanCours\" " +
                "href=\"https://planets.etsmtl.ca/public/Versionpdf.aspx?sigle=LLL111\">" +
                "Plan de cours" +
            "</a>");

        HtmlExtractorPayload htmlExtractorPayload = new HtmlExtractorPayload();
        htmlExtractorPayload.setDocument(document);

        CourseTransformerPayload courseTransformerPayload =
                courseFileTransformer.transform(htmlExtractorPayload);

        assertThat(courseTransformerPayload.getAcronym(), is(equalTo("LLL111")));
        assertThat(courseTransformerPayload.getDescription(), is(equalTo("Description")));
        assertThat(courseTransformerPayload.getCredits(), is(nullValue()));
        assertThat(courseTransformerPayload.getPrerequisites(), is(equalTo(Arrays.asList("AAA200", "BBB300"))));
    }

    @Test
    public void transform_emptyPrerequisites() throws Exception {

        Document document = Jsoup.parse(
                "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneCenter_FicheCours_LabelDescription\">" +
                    "Description" +
                "</div>" +
                "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_LabelCredits\">" +
                    "Crédits : 3 cr." +
                "</div>" +
                "<div id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_LabelPrealables\">" +
                "</div>" +
                "<a id=\"plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_HyperLinkPlanCours\" " +
                    "href=\"https://planets.etsmtl.ca/public/Versionpdf.aspx?sigle=LLL111\">" +
                    "Plan de cours" +
                "</a>");

        HtmlExtractorPayload htmlExtractorPayload = new HtmlExtractorPayload();
        htmlExtractorPayload.setDocument(document);

        CourseTransformerPayload courseTransformerPayload =
                courseFileTransformer.transform(htmlExtractorPayload);

        assertThat(courseTransformerPayload.getAcronym(), is(equalTo("LLL111")));
        assertThat(courseTransformerPayload.getDescription(), is(equalTo("Description")));
        assertThat(courseTransformerPayload.getCredits(), is(equalTo(3)));
        assertThat(courseTransformerPayload.getPrerequisites().size(), is(equalTo(0)));
    }
}