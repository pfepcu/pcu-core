package ca.pcuets.core.etl.transformer;

import ca.pcuets.core.etl.extractor.XlsExtractor;
import ca.pcuets.core.etl.extractor.payload.XlsExtractorPayload;
import ca.pcuets.core.etl.transformer.payload.CourseSessionTransformerPayload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class CourseSessionTransformerTest {

    @InjectMocks
    CourseSessionTransformer courseSessionTransformer;

    @Test
    public void transform() throws Exception {
        XlsExtractor xlsExtractor = new XlsExtractor();
        XlsExtractorPayload extractorPayload = xlsExtractor.extract("etl_test_files/etl_test_normal.xlsx");

        List<CourseSessionTransformerPayload> payloadList;
        payloadList = courseSessionTransformer.transform(extractorPayload);

        assertThat(payloadList.size(), is(equalTo(4)));

        assertThat(payloadList.get(0).getCourseTransformerPayload().getAcronym(), is(equalTo("LOG791")));
        assertThat(payloadList.get(0).getCourseTransformerPayload().getName(), is(equalTo("Projets spéciaux en génie logiciel")));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().size(), is(equalTo(5)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(0).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(0).getSession(), is(equalTo("E17")));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(1).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(1).getSession(), is(equalTo("A17")));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(2).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(2).getSession(), is(equalTo("H18")));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(3).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(3).getSession(), is(equalTo("E18")));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(4).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(4).getSession(), is(equalTo("A18")));

        assertThat(payloadList.get(1).getCourseTransformerPayload().getAcronym(), is(equalTo("LOG792")));
        assertThat(payloadList.get(1).getCourseTransformerPayload().getName(), is(equalTo("Projet de fin d'études en génie logiciel")));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().size(), is(equalTo(5)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(0).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(0).getSession(), is(equalTo("E17")));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(1).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(1).getSession(), is(equalTo("A17")));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(2).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(2).getSession(), is(equalTo("H18")));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(3).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(3).getSession(), is(equalTo("E18")));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(4).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(4).getSession(), is(equalTo("A18")));

        assertThat(payloadList.get(2).getCourseTransformerPayload().getAcronym(), is(equalTo("MAT210")));
        assertThat(payloadList.get(2).getCourseTransformerPayload().getName(), is(equalTo("Logique et mathématiques discrètes")));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().size(), is(equalTo(5)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(0).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(0).getSession(), is(equalTo("E17")));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(1).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(1).getSession(), is(equalTo("A17")));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(2).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(2).getEvening(), is(equalTo(true)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(2).getSession(), is(equalTo("H18")));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(3).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(3).getSession(), is(equalTo("E18")));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(4).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(4).getSession(), is(equalTo("A18")));

        assertThat(payloadList.get(3).getCourseTransformerPayload().getAcronym(), is(equalTo("MAT472")));
        assertThat(payloadList.get(3).getCourseTransformerPayload().getName(), is(equalTo("Algèbre linéaire et géométrie de l'espace")));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().size(), is(equalTo(5)));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(0).getEvening(), is(equalTo(true)));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(0).getSession(), is(equalTo("E17")));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(1).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(1).getSession(), is(equalTo("A17")));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(2).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(2).getSession(), is(equalTo("H18")));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(3).getEvening(), is(equalTo(true)));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(3).getSession(), is(equalTo("E18")));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(4).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(4).getSession(), is(equalTo("A18")));
    }

    @Test
    public void transform_sessionWithoutSchedule() throws Exception {
        XlsExtractor xlsExtractor = new XlsExtractor();
        XlsExtractorPayload extractorPayload = xlsExtractor.extract("etl_test_files/etl_test_no_schedule.xlsx");

        List<CourseSessionTransformerPayload> payloadList;
        payloadList = courseSessionTransformer.transform(extractorPayload);

        assertThat(payloadList.size(), is(equalTo(4)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().size(), is(equalTo(4)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(0).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(1).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(1).getEvening(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(2).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(2).getEvening(), is(equalTo(true)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().get(3).getDay(), is(equalTo(true)));

        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().size(), is(equalTo(2)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(0).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(0).getEvening(), is(equalTo(true)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().get(1).getDay(), is(equalTo(true)));

        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().size(), is(equalTo(3)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(0).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(1).getDay(), is(equalTo(true)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().get(2).getDay(), is(equalTo(true)));

        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().size(), is(equalTo(1)));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().get(0).getEvening(), is(equalTo(true)));
    }

    @Test
    public void transform_lineWithNoData() throws Exception {
        XlsExtractor xlsExtractor = new XlsExtractor();
        XlsExtractorPayload extractorPayload = xlsExtractor.extract("etl_test_files/etl_test_line_not_data.xlsx");

        List<CourseSessionTransformerPayload> payloadList;
        payloadList = courseSessionTransformer.transform(extractorPayload);

        assertThat(payloadList.size(), is(equalTo(4)));
        assertThat(payloadList.get(0).getSessionScheduleTransformerPayloads().size(), is(equalTo(5)));
        assertThat(payloadList.get(1).getSessionScheduleTransformerPayloads().size(), is(equalTo(5)));
        assertThat(payloadList.get(2).getSessionScheduleTransformerPayloads().size(), is(equalTo(5)));
        assertThat(payloadList.get(3).getSessionScheduleTransformerPayloads().size(), is(equalTo(5)));
    }

    @Test
    public void transform_courseWithAcronymExtraString() throws Exception {
        XlsExtractor xlsExtractor = new XlsExtractor();
        XlsExtractorPayload extractorPayload = xlsExtractor.extract("etl_test_files/etl_test_acronym.xlsx");

        List<CourseSessionTransformerPayload> payloadList;
        payloadList = courseSessionTransformer.transform(extractorPayload);

        assertThat(payloadList.size(), is(equalTo(4)));
        assertThat(payloadList.get(0).getCourseTransformerPayload().getAcronym(), is(equalTo("LOG530")));
        assertThat(payloadList.get(1).getCourseTransformerPayload().getAcronym(), is(equalTo("LOG540")));
        assertThat(payloadList.get(2).getCourseTransformerPayload().getAcronym(), is(equalTo("LOG550")));
        assertThat(payloadList.get(3).getCourseTransformerPayload().getAcronym(), is(equalTo("LOG710")));
    }

    @Test
    public void transform_courseNameWithParenthesis() throws Exception {
        XlsExtractor xlsExtractor = new XlsExtractor();
        XlsExtractorPayload extractorPayload = xlsExtractor.extract("etl_test_files/etl_test_parenthesis.xlsx");

        List<CourseSessionTransformerPayload> payloadList;
        payloadList = courseSessionTransformer.transform(extractorPayload);

        assertThat(payloadList.size(), is(equalTo(4)));
        assertThat(payloadList.get(0).getCourseTransformerPayload().getName(), is(equalTo("Conception et évaluation des interfaces utilisateurs ")));
        assertThat(payloadList.get(1).getCourseTransformerPayload().getName(), is(equalTo("Réseaux de communication IP ")));
        assertThat(payloadList.get(2).getCourseTransformerPayload().getName(), is(equalTo("Sécurité des systèmes ")));
        assertThat(payloadList.get(3).getCourseTransformerPayload().getName(), is(equalTo("Interfaces utilisateurs avancées ")));
    }

}