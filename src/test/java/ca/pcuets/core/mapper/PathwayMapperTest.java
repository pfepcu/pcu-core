package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Pathway;
import ca.pcuets.core.fixture.CourseEntityFixture;
import ca.pcuets.core.fixture.SessionCourseEntityFixture;
import ca.pcuets.core.fixture.SessionCourseStudentEntityFixture;
import ca.pcuets.core.fixture.SessionEntityFixture;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class PathwayMapperTest {

    private PathwayMapper pathwayMapper;

    @Before
    public void setup() {

        pathwayMapper = new PathwayMapper();
    }

    @Test
    public void mapFromList() throws Exception {

        SessionEntity sessionEntityH18 = SessionEntityFixture.get(SeasonEnum.WINTER, 2018);
        SessionEntity sessionEntityH19 = SessionEntityFixture.get(SeasonEnum.WINTER, 2019);

        SessionCourseEntity sessionCourseEntityAAA222 =
                SessionCourseEntityFixture.get(CourseEntityFixture.get("AAA-222", "Cours222", "Description222", 2),
                        sessionEntityH18);
        SessionCourseEntity sessionCourseEntityAAA333 =
                SessionCourseEntityFixture.get(CourseEntityFixture.get("AAA-333", "Cours333", "Description333", 3),
                        sessionEntityH18);
        SessionCourseEntity sessionCourseEntityAAA444 =
                SessionCourseEntityFixture.get(CourseEntityFixture.get("AAA-444", "Cours444", "Description444", 4),
                        sessionEntityH19);

        List<SessionCourseStudentEntity> sessionCourseStudentEntities = Arrays.asList(
                SessionCourseStudentEntityFixture.get(sessionCourseEntityAAA222),
                SessionCourseStudentEntityFixture.get(sessionCourseEntityAAA333),
                SessionCourseStudentEntityFixture.get(sessionCourseEntityAAA444)
        );

        Pathway pathway = pathwayMapper.map(sessionCourseStudentEntities);

        assertThat(pathway.getStudentSessions().get(0).getSeason(), is(equalTo(SeasonEnum.WINTER.getValue())));
        assertThat(pathway.getStudentSessions().get(0).getYear(), is(equalTo(2018)));
        assertThat(pathway.getStudentSessions().get(0).getSessionCourses(), hasSize(2));
        assertThat(pathway.getStudentSessions().get(0).getSessionCourses().get(0).getCourse().getAcronym(), is(equalTo("AAA-222")));
        assertThat(pathway.getStudentSessions().get(0).getSessionCourses().get(1).getCourse().getAcronym(), is(equalTo("AAA-333")));

        assertThat(pathway.getStudentSessions().get(1).getSeason(), is(equalTo(SeasonEnum.WINTER.getValue())));
        assertThat(pathway.getStudentSessions().get(1).getYear(), is(equalTo(2019)));
        assertThat(pathway.getStudentSessions().get(1).getSessionCourses(), hasSize(1));
        assertThat(pathway.getStudentSessions().get(1).getSessionCourses().get(0).getCourse().getAcronym(), is(equalTo("AAA-444")));
    }

    @Test
    public void mapFromMap() throws Exception {

        SessionEntity sessionEntityH18 = SessionEntityFixture.get(SeasonEnum.WINTER, 2018);

        SessionCourseEntity sessionCourseEntityAAA222 =
                SessionCourseEntityFixture.get(CourseEntityFixture.get("AAA-222", "Cours222", "Description222", 2),
                        sessionEntityH18);
        SessionCourseEntity sessionCourseEntityAAA333 =
                SessionCourseEntityFixture.get(CourseEntityFixture.get("AAA-333", "Cours333", "Description333", 3),
                        sessionEntityH18);

        List<SessionCourseStudentEntity> sessionCourseStudentEntitiesH18 = Arrays.asList(
                SessionCourseStudentEntityFixture.get(sessionCourseEntityAAA222),
                SessionCourseStudentEntityFixture.get(sessionCourseEntityAAA333)
        );

        Map<SessionEntity, List<SessionCourseStudentEntity>> studentSessionEntityMap = new HashMap<>();
        studentSessionEntityMap.put(sessionEntityH18, sessionCourseStudentEntitiesH18);

        Pathway pathway = pathwayMapper.map(studentSessionEntityMap);

        assertThat(pathway.getStudentSessions().get(0).getSeason(), is(equalTo(SeasonEnum.WINTER.getValue())));
        assertThat(pathway.getStudentSessions().get(0).getYear(), is(equalTo(2018)));
        assertThat(pathway.getStudentSessions().get(0).getSessionCourses(), hasSize(2));
        assertThat(pathway.getStudentSessions().get(0).getSessionCourses().get(0).getCourse().getAcronym(), is(equalTo("AAA-222")));
        assertThat(pathway.getStudentSessions().get(0).getSessionCourses().get(1).getCourse().getAcronym(), is(equalTo("AAA-333")));
    }

}