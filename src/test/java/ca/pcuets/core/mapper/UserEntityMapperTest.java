package ca.pcuets.core.mapper;

import ca.pcuets.core.dao.ConcentrationDAO;
import ca.pcuets.core.dao.ProgramDAO;
import ca.pcuets.core.dao.SessionDAO;
import ca.pcuets.core.dto.User;
import ca.pcuets.core.fixture.UserEntityFixture;
import ca.pcuets.core.fixture.UserFixture;
import ca.pcuets.core.model.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;

@RunWith(MockitoJUnitRunner.class)
public class UserEntityMapperTest {

    @InjectMocks
    UserEntityMapper userEntityMapper;

    @Mock
    ProgramDAO programDAO;

    @Mock
    ConcentrationDAO concentrationDAO;

    @Mock
    SessionDAO sessionDAO;

    @Test
    public void map() throws Exception {

        User user = new User();
        user.setEmail("email@email.email");
        user.setFirstName("Jon");
        user.setLastName("Sandman");
        user.setPassword("MotDePasseNonHaché");
        user.setProgramId(1);
        user.setConcentrationId(1);
        user.setStartingSessionId(1);

        UserEntity userEntity = userEntityMapper.map(user);

        assertThat(userEntity.getEmail(), is(equalTo("email@email.email")));
        assertThat(userEntity.getFirstName(), is(equalTo("Jon")));
        assertThat(userEntity.getLastName(), is(equalTo("Sandman")));
        assertThat(userEntity.getPassword(), not(equalTo("MotDePasseNonHaché")));
    }

    @Test
    public void map_updateUserEntity() throws Exception {

        User user = UserFixture.get("Jon", "Sandman", "email@email.email", "password");

        UserEntity existingUserEntity = UserEntityFixture.get(10, "bob@bob.bob", "password2");

        UserEntity userEntity = userEntityMapper.map(existingUserEntity, user);

        assertThat(userEntity.getEmail(), is(equalTo("bob@bob.bob")));
        assertThat(userEntity.getFirstName(), is(equalTo("Jon")));
        assertThat(userEntity.getLastName(), is(equalTo("Sandman")));
        assertThat(userEntity.getPassword(), not(equalTo("password")));
    }


}