package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Program;
import ca.pcuets.core.model.ProgramEntity;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class ProgramMapperTest {

    @Test
    public void map() throws Exception {

        ProgramEntity programEntity = new ProgramEntity();
        programEntity.setId(1);
        programEntity.setDescription("Description");
        programEntity.setName("Nom");

        ProgramMapper programMapper = new ProgramMapper();
        Program expectedProgram = programMapper.map(programEntity);

        assertThat(expectedProgram.getId(), is(equalTo(1)));
        assertThat(expectedProgram.getDescription(), is(equalTo("Description")));
        assertThat(expectedProgram.getName(), is(equalTo("Nom")));
    }

}