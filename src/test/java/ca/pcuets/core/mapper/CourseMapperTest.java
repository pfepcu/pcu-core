package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Course;
import ca.pcuets.core.fixture.CourseEntityFixture;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.CoursePrerequisiteEntity;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;


import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class CourseMapperTest {
    @Test
    public void map() throws Exception {

        CourseEntity successorEntity =
                CourseEntityFixture.get(1, "XXX212", "Nom Successor",
                                        "Description Successor", 4,
                                        1, null, new ArrayList<CoursePrerequisiteEntity>());

        CourseEntity prerequisiteEntity =
                CourseEntityFixture.get(2, "XXX111", "Nom Prerequisite",
                                        "Description Prerequisite", 5,
                                        2, null, new ArrayList<CoursePrerequisiteEntity>());

        CourseEntity courseEntity =
                CourseEntityFixture.get(3, "XXX211", "Nom",
                                        "Description", 6,
                                        3, successorEntity, new ArrayList<CoursePrerequisiteEntity>());

        CoursePrerequisiteEntity prerequisite = new CoursePrerequisiteEntity();
        prerequisite.setId(3);
        prerequisite.setCourse(courseEntity);
        prerequisite.setPrerequisite(prerequisiteEntity);

        courseEntity.getPrerequisites().add(prerequisite);

        CourseMapper courseMapper = new CourseMapper();
        Course expectedCourse = courseMapper.map(courseEntity);

        assertThat(expectedCourse.getId(), is(equalTo(3)));
        assertThat(expectedCourse.getDescription(), is(equalTo("Description")));
        assertThat(expectedCourse.getName(), is(equalTo("Nom")));
        assertThat(expectedCourse.getAcronym(), is(equalTo("XXX211")));
        assertThat(expectedCourse.getCredits(), is(equalTo(6)));
        assertThat(expectedCourse.getRequiredCredits(), is(equalTo(3)));

        assertThat(expectedCourse.getSuccessor().getId(), is(equalTo(1)));
        assertThat(expectedCourse.getSuccessor().getDescription(), is(equalTo("Description Successor")));
        assertThat(expectedCourse.getSuccessor().getName(), is(equalTo("Nom Successor")));
        assertThat(expectedCourse.getSuccessor().getAcronym(), is(equalTo("XXX212")));
        assertThat(expectedCourse.getSuccessor().getCredits(), is(equalTo(4)));
        assertThat(expectedCourse.getSuccessor().getRequiredCredits(), is(equalTo(1)));

        assertThat(expectedCourse.getPrerequisites().get(0).getId(), is(equalTo(2)));
        assertThat(expectedCourse.getPrerequisites().get(0).getDescription(), is(equalTo("Description Prerequisite")));
        assertThat(expectedCourse.getPrerequisites().get(0).getName(), is(equalTo("Nom Prerequisite")));
        assertThat(expectedCourse.getPrerequisites().get(0).getAcronym(), is(equalTo("XXX111")));
        assertThat(expectedCourse.getPrerequisites().get(0).getCredits(), is(equalTo(5)));
        assertThat(expectedCourse.getPrerequisites().get(0).getRequiredCredits(), is(equalTo(2)));

    }

}