package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Session;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class SessionMapperTest {

    @InjectMocks
    private SessionMapper sessionMapper;

    @Test
    public void map() throws Exception {

        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setYear(2017);
        sessionEntity.setSeason(SeasonEnum.WINTER);

        Session session = sessionMapper.map(sessionEntity);

        assertThat(session.getYear(), is(equalTo(2017)));
        assertThat(session.getSeason(), is(equalTo(SeasonEnum.WINTER.getValue())));
    }

}