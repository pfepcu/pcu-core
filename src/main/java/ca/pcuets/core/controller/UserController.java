package ca.pcuets.core.controller;

import ca.pcuets.core.dao.PreferenceDAO;
import ca.pcuets.core.dao.UserDAO;
import ca.pcuets.core.dto.User;
import ca.pcuets.core.dto.validationlevel.CreateValidationLevel;
import ca.pcuets.core.dto.validationlevel.UpdateValidationLevel;
import ca.pcuets.core.factory.PreferenceEntityFactory;
import ca.pcuets.core.mapper.UserEntityMapper;
import ca.pcuets.core.model.PreferenceEntity;
import ca.pcuets.core.model.UserEntity;
import ca.pcuets.core.helper.SessionHelper;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/users", consumes = "application/json", produces = "application/json")
public class UserController {

    @Resource
    private UserDAO userDAO;

    @Resource
    private PreferenceDAO preferenceDAO;

    @Resource
    private UserEntityMapper userEntityMapper;

    @Resource
    private PreferenceEntityFactory preferenceEntityFactory;

    @Resource
    private SessionHelper sessionHelper;

    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody @Validated(CreateValidationLevel.class) User user) {

        validateEmailDisponibility(user.getEmail());

        UserEntity userEntity = userEntityMapper.map(user);
        List<PreferenceEntity> preferences = preferenceEntityFactory.createDefaultPreference(userEntity);
        userEntity.setPreferenceEntities(preferences);
        userDAO.save(userEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable("id") Integer id,
                       @RequestBody @Validated(UpdateValidationLevel.class) User user) {

        sessionHelper.validateSessionUserId(id);

        Optional<UserEntity> userEntityOptional = userDAO.findById(id);
        if (!userEntityOptional.isPresent()) {
            throw new IllegalArgumentException("Can't find user associated with sent id");
        }

        UserEntity updatedUserEntity = userEntityMapper.map(userEntityOptional.get(), user);
        userDAO.save(updatedUserEntity);
    }

    private void validateEmailDisponibility(String email) {

        Optional<UserEntity> userEntityOptional = userDAO.findByEmail(email);

        if (userEntityOptional.isPresent()) {
            throw new IllegalArgumentException("Associated email already exists");
        }
    }
}