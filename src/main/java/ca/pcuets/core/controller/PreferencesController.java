package ca.pcuets.core.controller;

import ca.pcuets.core.dao.PreferenceDAO;
import ca.pcuets.core.dto.Preference;
import ca.pcuets.core.helper.SessionHelper;
import ca.pcuets.core.mapper.PreferenceEntityMapper;
import ca.pcuets.core.mapper.PreferenceMapper;
import ca.pcuets.core.model.PreferenceEntity;
import ca.pcuets.core.model.UserEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/preferences", consumes = "application/json", produces = "application/json")
public class PreferencesController {

    @Resource
    private SessionHelper sessionHelper;

    @Resource
    private PreferenceMapper preferenceMapper;

    @Resource
    private PreferenceEntityMapper preferenceEntityMapper;

    @Resource
    private PreferenceDAO preferenceDAO;

    @RequestMapping(method = RequestMethod.GET)
    public Preference getPreferences() {
        UserEntity userEntity = sessionHelper.getSessionUser();
        return userEntity.getPreferenceEntities().stream().map(preferenceEntity -> preferenceMapper.map(preferenceEntity)).findFirst().get();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Preference updatePreference(@RequestBody Preference preference) {
        UserEntity userEntity = sessionHelper.getSessionUser();

        PreferenceEntity preferenceEntity = preferenceEntityMapper.map(preference);
        preferenceEntity.setUser(userEntity);

        preferenceDAO.save(preferenceEntity);
        return preference;
    }

    @RequestMapping(method = RequestMethod.POST)
    public Preference createPreference(@RequestBody Preference preference) {
        UserEntity userEntity = sessionHelper.getSessionUser();

        PreferenceEntity preferenceEntity = preferenceEntityMapper.map(preference);
        preferenceEntity.setUser(userEntity);
        preferenceEntity = preferenceDAO.save(preferenceEntity);

        preference.setId(preferenceEntity.getId());

        return preference;
    }

}
