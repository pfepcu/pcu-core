package ca.pcuets.core.controller;

import ca.pcuets.core.dao.ConcentrationDAO;
import ca.pcuets.core.dto.Concentration;
import ca.pcuets.core.mapper.ConcentrationMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/concentrations", consumes = "application/json", produces = "application/json")
public class ConcentrationController {

    @Resource
    private ConcentrationDAO concentrationDAO;

    @Resource
    private ConcentrationMapper concentrationMapper;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<Concentration> getAllConcentrations() {

        return concentrationDAO.findAll().stream().map(concentrationEntity ->
                concentrationMapper.map(concentrationEntity)).collect(Collectors.toList());
    }
}
