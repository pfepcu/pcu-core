package ca.pcuets.core.controller;

import ca.pcuets.core.dao.UserDAO;
import ca.pcuets.core.dto.Pathway;
import ca.pcuets.core.dto.PathwayGenerationRequest;
import ca.pcuets.core.helper.SessionHelper;
import ca.pcuets.core.mapper.PathwayMapper;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.UserEntity;
import ca.pcuets.core.service.PathwayService;
import ca.pcuets.core.service.SessionCourseStudentService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/pathways", consumes = "application/json", produces = "application/json")
public class PathwayController {

    @Resource
    private SessionHelper sessionHelper;

    @Resource
    private PathwayService pathwayService;

    @Resource
    private PathwayMapper pathwayMapper;

    @Resource
    private UserDAO userDAO;

    @Resource
    private SessionCourseStudentService sessionCourseStudentService;

    @RequestMapping(method = RequestMethod.GET)
    public Pathway get() {

        UserEntity student = sessionHelper.getSessionUser();
        return pathwayMapper.map(student.getSessionCourses());
    }

    @RequestMapping(value = "/generate", method = RequestMethod.PUT)
    public Pathway generate(@RequestBody @Valid PathwayGenerationRequest pathwayGenerationRequest) {

        UserEntity student = sessionHelper.getSessionUser();

        Map<SessionEntity, List<SessionCourseStudentEntity>> sessionCoursesPerSession =
                pathwayService.generate(student,
                        pathwayGenerationRequest.getStartingSessionId(),
                        pathwayGenerationRequest.getNextInternshipSessionId());

        List<SessionCourseStudentEntity> sessionCourseStudents = sessionCoursesPerSession.entrySet().stream()
                .flatMap(sessionEntityListEntry -> sessionEntityListEntry.getValue().stream())
                .collect(Collectors.toList());

        student.clearSessionCourseStudents();
        student.addSessionCourseStudents(sessionCourseStudents);
        userDAO.save(student);

        return pathwayMapper.map(student.getSessionCourses());
    }

    @RequestMapping(value = "/lockcourse/{id}", method = RequestMethod.PUT)
    public void lockCourseStudent(@PathVariable("id") Integer id) {
        sessionCourseStudentService.setLockedCourse(id, true);
    }

    @RequestMapping(value = "/unlockcourse/{id}", method = RequestMethod.PUT)
    public void unlockCourseStudent(@PathVariable("id") Integer id) {
        sessionCourseStudentService.setLockedCourse(id, false);
    }

    @RequestMapping(value = "/locksession/{id}", method = RequestMethod.PUT)
    public void lockSessionStudent(@PathVariable("id") Integer id) {
        sessionCourseStudentService.setLockedSession(id, true);
    }

    @RequestMapping(value = "/unlocksession/{id}", method = RequestMethod.PUT)
    public void unlockSessionStudent(@PathVariable("id") Integer id) {
        sessionCourseStudentService.setLockedSession(id, false);
    }
}
