package ca.pcuets.core.controller;

import ca.pcuets.core.dao.ProgramDAO;
import ca.pcuets.core.dto.Program;
import ca.pcuets.core.mapper.ProgramMapper;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/programs", consumes = "application/json", produces = "application/json")
public class ProgramController {

    @Resource
    private ProgramDAO programDAO;

    @Resource
    private ProgramMapper programMapper;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<Program> getAllPrograms() {

        return programDAO.findAll().stream().map(programEntity ->
                programMapper.map(programEntity)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Program getProgram(@PathVariable("id") Integer id) {
        return programMapper.map(programDAO.findById(id));
    }

}
