package ca.pcuets.core.controller;

import ca.pcuets.core.dao.SessionDAO;
import ca.pcuets.core.dto.Session;
import ca.pcuets.core.mapper.SessionMapper;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sessions", consumes = "application/json", produces = "application/json")
public class SessionController {

    @Resource
    SessionDAO sessionDAO;

    @Resource
    SessionMapper sessionMapper;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<Session> getAllSessions() {

        return sessionDAO.findAll().stream()
                .filter(sessionEntity -> !sessionEntity.getYear().equals(0))
                .map(sessionEntity -> sessionMapper.map(sessionEntity))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Session getProgram(@PathVariable("id") Integer id) {
        return sessionMapper.map(sessionDAO.findById(id));
    }

}
