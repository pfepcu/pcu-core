package ca.pcuets.core.controller;

import ca.pcuets.core.dao.UserDAO;
import ca.pcuets.core.dto.Credential;
import ca.pcuets.core.dto.User;
import ca.pcuets.core.helper.SessionHelper;
import ca.pcuets.core.mapper.UserMapper;
import ca.pcuets.core.model.UserEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = "/auth", consumes = "application/json", produces = "application/json")
public class AuthenticationController {

    @Resource
    private UserDAO userDao;

    @Resource
    private UserMapper userMapper;

    @Resource
    private SessionHelper sessionHelper;

    @Value("${webapp.url}")
    private String webappUrl;

    @RequestMapping(value = "me", method = RequestMethod.GET)
    public User getLoggedUser() {

        Optional<UserEntity> userEntityOptional = userDao.findById(sessionHelper.getSessionUserId());

        if(!userEntityOptional.isPresent()) {

            throw new IllegalArgumentException("Session user infos are bad");
        }

        return userMapper.map(userEntityOptional.get());
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public void login(@RequestBody @Valid Credential credential) {

        Optional<UserEntity> userEntityOptional = userDao.findByEmail(credential.getEmail());

        if(!userEntityOptional.isPresent()) {
            throw new IllegalArgumentException("Invalid credentials");
        }

        if(!BCrypt.checkpw(credential.getPassword(), userEntityOptional.get().getPassword())) {
            throw new IllegalArgumentException("Invalid credentials");
        }

        sessionHelper.createSession(credential, userEntityOptional.get().getId());
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response) {

        HttpSession httpSession = request.getSession(false);
        httpSession.invalidate();

        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(null);

        SecurityContextHolder.clearContext();

        removeCookies(request, response);
        setHeaders(response);
    }

    private void setHeaders(HttpServletResponse response) {

        response.setHeader("Access-Control-Allow-Origin", webappUrl);
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        response.setHeader("Access-Control-Allow-Credentials", "true");
    }

    private void removeCookies(HttpServletRequest request, HttpServletResponse response) {

        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            cookie.setMaxAge(0);
            cookie.setValue(null);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
    }
}
