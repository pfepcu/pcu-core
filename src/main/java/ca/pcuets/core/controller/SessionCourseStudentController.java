package ca.pcuets.core.controller;

import ca.pcuets.core.dto.SessionCourse;
import ca.pcuets.core.helper.SessionHelper;
import ca.pcuets.core.mapper.SessionCourseMapper;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.UserEntity;
import ca.pcuets.core.service.CourseService;
import ca.pcuets.core.service.SessionCourseStudentService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/sessionCourseStudents", consumes = "application/json", produces = "application/json")
public class SessionCourseStudentController {

    @Resource
    private SessionHelper sessionHelper;

    @Resource
    private CourseService courseService;

    @Resource
    private SessionCourseStudentService sessionCourseStudentService;

    @Resource
    private SessionCourseMapper sessionCourseMapper;

    @RequestMapping(path = "/past", method = RequestMethod.GET)
    public List<SessionCourse> getPastSessionCourseStudents() {

        UserEntity student = sessionHelper.getSessionUser();
        List<SessionCourseStudentEntity> pastSessionCourseStudents = sessionCourseStudentService.findPastSessionCourseStudents(student);

        List<SessionCourseEntity> sessionCourses = pastSessionCourseStudents.stream()
                .map(SessionCourseStudentEntity::getSessionCourse)
                .collect(Collectors.toList());

        return sessionCourseMapper.map(sessionCourses);

    }

    @RequestMapping(path = "/past/{courseId}", method = RequestMethod.POST)
    public void createPastSessionCourseStudent(@PathVariable("courseId") Integer courseId) {

        UserEntity student = sessionHelper.getSessionUser();
        CourseEntity courseEntity = courseService.findById(courseId);

        if(sessionCourseStudentService.findPastSessionCourseStudent(student, courseEntity) != null) {
            throw new IllegalArgumentException("Past course already added");
        }

        sessionCourseStudentService.createPastSessionCourseStudent(student, courseEntity);
    }

    @RequestMapping(path = "/past/{courseId}", method = RequestMethod.DELETE)
    public void deletePastSessionCourseStudent(@PathVariable("courseId") Integer courseId) {

        UserEntity student = sessionHelper.getSessionUser();
        CourseEntity courseEntity = courseService.findById(courseId);
        sessionCourseStudentService.deletePastSessionCourseStudent(student, courseEntity);
    }
}
