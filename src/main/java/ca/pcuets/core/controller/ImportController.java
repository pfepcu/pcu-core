package ca.pcuets.core.controller;

import ca.pcuets.core.etl.MattDeamon;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/imports")
public class ImportController {

    @Resource
    private MattDeamon mattDeamon;

    @RequestMapping(method = RequestMethod.GET)
    public void importData() {
        mattDeamon.executeImportRoutine();
    }
}
