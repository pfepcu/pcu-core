package ca.pcuets.core.controller;

import ca.pcuets.core.dao.CourseDAO;
import ca.pcuets.core.dto.Course;
import ca.pcuets.core.mapper.CourseMapper;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/courses", consumes = "application/json", produces = "application/json")
public class CourseController {

    @Resource
    private CourseDAO courseDAO;

    @Resource
    private CourseMapper courseMapper;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Course getCourse(@PathVariable("id") Integer id) {
        return courseMapper.map(courseDAO.findById(id));
    }

}
