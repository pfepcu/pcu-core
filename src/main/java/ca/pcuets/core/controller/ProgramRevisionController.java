package ca.pcuets.core.controller;

import ca.pcuets.core.dto.Course;
import ca.pcuets.core.helper.SessionHelper;
import ca.pcuets.core.mapper.CourseMapper;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.ProgramRevisionEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.service.ProgramRevisionService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/programRevisions", consumes = "application/json", produces = "application/json")
public class ProgramRevisionController {

    @Resource
    private ProgramRevisionService programRevisionService;

    @Resource
    private CourseMapper courseMapper;

    @Resource
    private SessionHelper sessionHelper;

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public List<Course> getUserProgramRevisionCourses() {

        SessionEntity session = sessionHelper.getSessionUser().getStartingSession();
        ProgramRevisionEntity programRevision = programRevisionService.findByStartingYearAndSeason(session.getYear(), session.getSeason());
        List<CourseEntity> courses = programRevisionService.getCourses(programRevision);

        return courses.stream()
                .map(courseEntity -> courseMapper.map(courseEntity))
                .collect(Collectors.toList());
    }
}
