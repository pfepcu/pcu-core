package ca.pcuets.core.dao;

import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.UserEntity;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface SessionCourseStudentDAO extends CrudRepository<SessionCourseStudentEntity, Long> {

    SessionCourseStudentEntity findById(Integer id);

    SessionCourseStudentEntity findByUserAndSessionCourse(UserEntity user, SessionCourseEntity sessionCourse);

    List<SessionCourseStudentEntity> findByUserAndSessionCourseSessionId(UserEntity user, Integer id);
}
