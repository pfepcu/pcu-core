package ca.pcuets.core.dao;

import ca.pcuets.core.model.ProgramRevisionEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProgramRevisionDAO extends CrudRepository<ProgramRevisionEntity, Long> {

    ProgramRevisionEntity findById(Integer id);

    @Query(value =  "SELECT pr " +
                    "FROM " +
                    "   ProgramRevisionEntity pr " +
                    "   LEFT JOIN pr.startingSession " +
                    "   LEFT JOIN pr.endingSession " +
                    "WHERE ( " +
                    "    pr.startingSession IS NULL " +
                    "    OR " +
                    "    pr.startingSession IN (SELECT s FROM SessionEntity s WHERE (s.year = :year AND s.season <= :season) OR s.year < :year) " +
                    ") AND (" +
                    "    pr.endingSession IS NULL " +
                    "    OR " +
                    "    pr.endingSession IN (SELECT s FROM SessionEntity s WHERE (s.year = :year AND s.season >= :season) OR s.year > :year) " +
                    ")")
    ProgramRevisionEntity findRevisionByYearAndSeason(@Param("year")Integer year, @Param("season")SeasonEnum season);
}
