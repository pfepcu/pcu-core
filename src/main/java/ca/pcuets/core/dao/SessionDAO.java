package ca.pcuets.core.dao;

import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SessionDAO extends CrudRepository<SessionEntity, Long> {

    List<SessionEntity> findAll();

    SessionEntity findById(Integer id);

    SessionEntity findBySeasonAndYear(SeasonEnum season, Integer year);

    SessionEntity findByYear(Integer year);
}
