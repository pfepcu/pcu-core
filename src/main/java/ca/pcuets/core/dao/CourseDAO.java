package ca.pcuets.core.dao;

import ca.pcuets.core.model.CourseEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CourseDAO extends CrudRepository<CourseEntity, Long> {

    List<CourseEntity> findAll();

    CourseEntity findById(Integer id);

    CourseEntity findByAcronym(String acronym);
}
