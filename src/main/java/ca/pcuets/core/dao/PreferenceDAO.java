package ca.pcuets.core.dao;

import ca.pcuets.core.model.PreferenceEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PreferenceDAO  extends CrudRepository<PreferenceEntity, Long> {

    PreferenceEntity findById(Integer id);

    List<PreferenceEntity> findByUserId(Integer userId);

}
