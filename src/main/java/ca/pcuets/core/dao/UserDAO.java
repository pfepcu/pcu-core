package ca.pcuets.core.dao;

import ca.pcuets.core.model.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDAO extends CrudRepository<UserEntity, Long> {

    Optional<UserEntity> findById(Integer id);

    Optional<UserEntity> findByEmail(String email);
}
