package ca.pcuets.core.dao;

import ca.pcuets.core.model.ProgramEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProgramDAO extends CrudRepository<ProgramEntity, Long> {

    List<ProgramEntity> findAll();

    ProgramEntity findById(Integer id);
}
