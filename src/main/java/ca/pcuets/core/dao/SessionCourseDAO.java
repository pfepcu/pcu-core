package ca.pcuets.core.dao;

import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SessionCourseDAO extends CrudRepository<SessionCourseEntity, Long> {

    List<SessionCourseEntity> findAll();

    SessionCourseEntity findById(Integer id);

    SessionCourseEntity findByCourseAndSession(CourseEntity courseEntity, SessionEntity sessionEntity);
}
