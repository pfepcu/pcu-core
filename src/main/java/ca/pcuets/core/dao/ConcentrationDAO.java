package ca.pcuets.core.dao;

import ca.pcuets.core.model.ConcentrationEntity;
import ca.pcuets.core.model.ProgramEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConcentrationDAO extends CrudRepository<ConcentrationEntity, Long>{

    List<ConcentrationEntity> findAll();

    ConcentrationEntity findById(Integer id);
}
