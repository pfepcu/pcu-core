package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Concentration;
import ca.pcuets.core.model.ConcentrationEntity;
import org.springframework.stereotype.Component;

@Component
public class ConcentrationMapper {

    public Concentration map(ConcentrationEntity concentrationEntity){

        Concentration concentration = new Concentration();
        concentration.setId(concentrationEntity.getId());
        concentration.setIdProgram(concentrationEntity.getProgram().getId());
        concentration.setDescription(concentrationEntity.getDescription());

        return concentration;
    }
}
