package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Preference;
import ca.pcuets.core.model.PreferenceEntity;
import org.springframework.stereotype.Component;

@Component
public class PreferenceEntityMapper {
    public PreferenceEntity map(Preference preference) {
        PreferenceEntity preferenceEntity = new PreferenceEntity();
        preferenceEntity.setId(preference.getId());
        preferenceEntity.setCourseDuringInternship(preference.getCourseDuringInternship());
        preferenceEntity.setMaxCourseDuringInternship(preference.getMaxCourseDuringInternship());
        preferenceEntity.setMaxEveningCourse(preference.getMaxEveningCourse());
        preferenceEntity.setMaxSessionCourse(preference.getMaxSessionCourse());
        preferenceEntity.setMaxSessionToComplete(preference.getMaxSessionToComplete());
        preferenceEntity.setMinSessionCourse(preference.getMinSessionCourse());
        preferenceEntity.setSessionBetweenInternship(preference.getSessionBetweenInternship());
        preferenceEntity.setEagerInternship(preference.getEagerInternship());

        return preferenceEntity;
    }
}
