package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.User;
import ca.pcuets.core.model.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User map(UserEntity userEntity) {

        User user = new User();
        user.setFirstName(userEntity.getFirstName());
        user.setLastName(userEntity.getLastName());
        user.setEmail(userEntity.getEmail());
        user.setStartingSessionId(userEntity.getStartingSession().getId());
        user.setProgramId(userEntity.getProgram().getId());

        return user;
    }
}
