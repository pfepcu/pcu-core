package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Course;
import ca.pcuets.core.dto.Pathway;
import ca.pcuets.core.dto.SessionCourse;
import ca.pcuets.core.dto.StudentSession;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.SessionEntity;
import java.util.Collections;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PathwayMapper {

    public Pathway map(List<SessionCourseStudentEntity> sessionCourseStudentEntities) {

        Map<SessionEntity, List<SessionCourseStudentEntity>> studentSessionEntityMap =
                sessionCourseStudentEntities.stream()
                    .collect(Collectors.groupingBy(o -> o.getSessionCourse().getSession()));

        return map(studentSessionEntityMap);
    }

    public Pathway map(Map<SessionEntity, List<SessionCourseStudentEntity>> studentSessionEntityMap) {

        List<StudentSession> studentSessions = studentSessionEntityMap.entrySet().stream()
                .map(this::getStudentSession)
                .collect(Collectors.toList());

        Pathway pathway = new Pathway();
        Collections.sort(studentSessions);
        pathway.setStudentSessions(studentSessions);
        return pathway;
    }

    private StudentSession getStudentSession(Map.Entry<SessionEntity, List<SessionCourseStudentEntity>> sessionEntityListEntry) {

        SessionEntity sessionEntity = sessionEntityListEntry.getKey();
        List<SessionCourse> sessionCourses = sessionEntityListEntry.getValue().stream()
                .map(this::getSessionCourse)
                .collect(Collectors.toList());

        StudentSession studentSession = new StudentSession();
        studentSession.setId(sessionEntity.getId());
        studentSession.setSeason(sessionEntity.getSeason().getValue());
        studentSession.setYear(sessionEntity.getYear());
        studentSession.setSessionCourses(sessionCourses);

        return studentSession;
    }

    private SessionCourse getSessionCourse(SessionCourseStudentEntity sessionCourseStudentEntity) {

        SessionCourseEntity sessionCourseEntity = sessionCourseStudentEntity.getSessionCourse();
        CourseEntity courseEntity = sessionCourseEntity.getCourse();

        Course course = new Course();
        course.setId(courseEntity.getId());
        course.setAcronym(courseEntity.getAcronym());
        course.setName(courseEntity.getName());

        SessionCourse sessionCourse = new SessionCourse();
        sessionCourse.setId(sessionCourseStudentEntity.getId());
        sessionCourse.setCourse(course);
        sessionCourse.setDay(sessionCourseEntity.getDay());
        sessionCourse.setEvening(sessionCourseEntity.getEvening());
        sessionCourse.setLocked(sessionCourseStudentEntity.getLocked());

        return sessionCourse;
    }
}
