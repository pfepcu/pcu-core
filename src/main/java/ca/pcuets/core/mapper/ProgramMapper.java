package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Program;
import ca.pcuets.core.model.ProgramEntity;
import org.springframework.stereotype.Component;

@Component
public class ProgramMapper {

    public Program map(ProgramEntity programEntity) {

        Program program = new Program();
        program.setId(programEntity.getId());
        program.setName(programEntity.getName());
        program.setDescription(programEntity.getDescription());

        return program;
    }
}
