package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Session;
import ca.pcuets.core.model.SessionEntity;
import org.springframework.stereotype.Component;

@Component
public class SessionMapper {

    public Session map(SessionEntity sessionEntity) {

        Session session = new Session();
        session.setId(sessionEntity.getId());
        session.setSeason(sessionEntity.getSeason().getValue());
        session.setYear(sessionEntity.getYear());

        return session;
    }
}
