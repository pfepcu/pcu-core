package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Course;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.CoursePrerequisiteEntity;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CourseMapper {

    public Course map(CourseEntity courseEntity) {

        Course course = new Course();
        course.setId(courseEntity.getId());
        course.setAcronym(courseEntity.getAcronym());
        course.setName(courseEntity.getName());
        course.setDescription(courseEntity.getDescription());
        course.setCredits(courseEntity.getCredits());
        course.setRequiredCredits(courseEntity.getRequiredCredits());

        if(courseEntity.getSuccessor() != null) {
            course.setSuccessor(map(courseEntity.getSuccessor()));
        }

        List<Course> prerequisites = new ArrayList();
        for (CoursePrerequisiteEntity coursePrerequisiteEntity:courseEntity.getPrerequisites()) {
            prerequisites.add(map(coursePrerequisiteEntity.getPrerequisite()));
        }

        course.setPrerequisites(prerequisites);

        return course;
    }
}
