package ca.pcuets.core.mapper;

import ca.pcuets.core.dao.ConcentrationDAO;
import ca.pcuets.core.dao.ProgramDAO;
import ca.pcuets.core.dao.SessionDAO;
import ca.pcuets.core.dto.User;
import ca.pcuets.core.model.UserEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
public class UserEntityMapper {

    @Resource
    private ProgramDAO programDAO;

    @Resource
    private SessionDAO sessionDAO;

    @Resource
    private ConcentrationDAO concentrationDAO;

    private static Integer BCRYPT_ROUNDS = 5;

    public UserEntity map(User user) {

        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        userEntity.setSuccessMattest(user.getSuccessMatTest());
        userEntity.setSuccessPhytest(user.getSuccessPhyTest());
        userEntity.setSuccessInftest(user.getSuccessInfTest());
        userEntity.setEmail(user.getEmail());
        userEntity.setProgram(programDAO.findById(user.getProgramId()));
        userEntity.setConcentration(concentrationDAO.findById(user.getConcentrationId()));
        userEntity.setStartingSession(sessionDAO.findById(user.getStartingSessionId()));
        userEntity.setCreationTime(new Date());
        userEntity.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(BCRYPT_ROUNDS)));
        return userEntity;
    }

    public UserEntity map(UserEntity userEntity, User user) {

        if(!user.getFirstName().isEmpty() && !user.getFirstName().equals(userEntity.getFirstName())) {
            userEntity.setFirstName(user.getFirstName());
        }

        if(!user.getLastName().isEmpty() && !user.getLastName().equals(userEntity.getLastName())) {
            userEntity.setLastName(user.getLastName());
        }

        if(!user.getPassword().isEmpty()) {
            userEntity.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(BCRYPT_ROUNDS)));
        }

        return userEntity;
    }
}
