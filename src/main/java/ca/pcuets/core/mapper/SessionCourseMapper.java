package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.SessionCourse;
import ca.pcuets.core.model.SessionCourseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SessionCourseMapper {

    @Resource
    private CourseMapper courseMapper;

    public List<SessionCourse> map(List<SessionCourseEntity> sessionCourseEntities) {

        return sessionCourseEntities.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    public SessionCourse map(SessionCourseEntity sessionCourseEntity) {

        SessionCourse sessionCourse = new SessionCourse();
        sessionCourse.setCourse(courseMapper.map(sessionCourseEntity.getCourse()));
        return sessionCourse;
    }
}
