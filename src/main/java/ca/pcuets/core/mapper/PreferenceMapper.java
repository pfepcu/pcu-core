package ca.pcuets.core.mapper;

import ca.pcuets.core.dto.Preference;
import ca.pcuets.core.model.PreferenceEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PreferenceMapper {

    public Preference map(PreferenceEntity preferenceEntity) {
        Preference preference = new Preference();
        preference.setId(preferenceEntity.getId());
        preference.setCourseDuringInternship(preferenceEntity.getCourseDuringInternship());
        preference.setMaxCourseDuringInternship(preferenceEntity.getMaxCourseDuringInternship());
        preference.setMaxEveningCourse(preferenceEntity.getMaxEveningCourse());
        preference.setMaxSessionCourse(preferenceEntity.getMaxSessionCourse());
        preference.setMaxSessionToComplete(preferenceEntity.getMaxSessionToComplete());
        preference.setMinSessionCourse(preferenceEntity.getMinSessionCourse());
        preference.setSessionBetweenInternship(preferenceEntity.getSessionBetweenInternship());
        preference.setEagerInternship(preferenceEntity.getEagerInternship());

        return preference;
    }
}
