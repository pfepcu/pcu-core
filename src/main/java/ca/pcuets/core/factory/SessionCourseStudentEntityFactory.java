package ca.pcuets.core.factory;

import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.UserEntity;
import ca.pcuets.core.model.enums.SessionCourseStudentStateEnum;
import org.springframework.stereotype.Service;

@Service
public class SessionCourseStudentEntityFactory {

    public SessionCourseStudentEntity get(UserEntity student, SessionCourseEntity sessionCourse,
                                          SessionCourseStudentStateEnum sessionCourseStudentState,
                                          Boolean locked) {

        SessionCourseStudentEntity sessionCourseStudentEntity = new SessionCourseStudentEntity();
        sessionCourseStudentEntity.setUser(student);
        sessionCourseStudentEntity.setSessionCourse(sessionCourse);
        sessionCourseStudentEntity.setState(sessionCourseStudentState);
        sessionCourseStudentEntity.setLocked(locked);
        return sessionCourseStudentEntity;
    }

    public SessionCourseStudentEntity get(UserEntity student, SessionCourseEntity sessionCourse, Boolean locked) {

        SessionCourseStudentEntity sessionCourseStudentEntity = new SessionCourseStudentEntity();
        sessionCourseStudentEntity.setUser(student);
        sessionCourseStudentEntity.setSessionCourse(sessionCourse);
        sessionCourseStudentEntity.setLocked(locked);
        return sessionCourseStudentEntity;
    }
}
