package ca.pcuets.core.factory;

import ca.pcuets.core.model.PreferenceEntity;
import ca.pcuets.core.model.UserEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PreferenceEntityFactory {

    public List<PreferenceEntity> createDefaultPreference(UserEntity userEntity) {

        PreferenceEntity preferenceEntity = new PreferenceEntity();
        preferenceEntity.setUser(userEntity);
        preferenceEntity.setSessionBetweenInternship(2);
        preferenceEntity.setMaxEveningCourse(2);
        preferenceEntity.setMaxCourseDuringInternship(0);
        preferenceEntity.setCourseDuringInternship(false);
        preferenceEntity.setMinSessionCourse(4);
        preferenceEntity.setEagerInternship(true);
        preferenceEntity.setMaxSessionCourse(4);
        preferenceEntity.setMaxSessionToComplete(11);

        List<PreferenceEntity> preferences = new ArrayList();
        preferences.add(preferenceEntity);

        return preferences;
    }

}
