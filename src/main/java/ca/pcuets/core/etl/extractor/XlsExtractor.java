package ca.pcuets.core.etl.extractor;

import ca.pcuets.core.etl.extractor.payload.XlsExtractorPayload;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class XlsExtractor implements Extractor{

    @Override
    public XlsExtractorPayload extract(String resourcePath) throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
        XlsExtractorPayload xlsExtractorPayload = new XlsExtractorPayload();

        File file = new File(classLoader.getResource(resourcePath).getFile());
        Workbook workbook = WorkbookFactory.create(file);
        xlsExtractorPayload.setWorkBook(workbook);

        return xlsExtractorPayload;
    }
}
