package ca.pcuets.core.etl.extractor.payload;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.jsoup.nodes.Document;

import javax.annotation.ManagedBean;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class HtmlExtractorPayload implements ExtractorPayload {

    private Document document;
}
