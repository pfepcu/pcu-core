package ca.pcuets.core.etl.extractor.payload;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.poi.ss.usermodel.Workbook;

import javax.annotation.ManagedBean;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class XlsExtractorPayload implements ExtractorPayload {

    private Workbook workBook;
}
