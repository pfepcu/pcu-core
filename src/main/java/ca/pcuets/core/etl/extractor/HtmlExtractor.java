package ca.pcuets.core.etl.extractor;

import ca.pcuets.core.etl.extractor.payload.HtmlExtractorPayload;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
public class HtmlExtractor implements Extractor {

    @Override
    public HtmlExtractorPayload extract(String resourcePath) throws Exception {

        HtmlExtractorPayload htmlPayload = new HtmlExtractorPayload();

        Document document = Jsoup.connect(resourcePath).get();
        htmlPayload.setDocument(document);

        return htmlPayload;
    }
}
