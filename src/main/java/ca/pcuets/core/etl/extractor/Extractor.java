package ca.pcuets.core.etl.extractor;

import ca.pcuets.core.etl.extractor.payload.ExtractorPayload;

public interface Extractor {

    ExtractorPayload extract(String resourcePath) throws Exception;
}
