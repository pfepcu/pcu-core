package ca.pcuets.core.etl.loader;

import ca.pcuets.core.dao.CourseDAO;
import ca.pcuets.core.etl.transformer.payload.CourseTransformerPayload;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.CoursePrerequisiteEntity;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CourseLoader implements Loader<CourseTransformerPayload> {

    @Resource
    private CourseDAO courseDAO;

    @Override
    public void load(CourseTransformerPayload courseTransformerPayload) throws Exception {

        String courseAcronym = courseTransformerPayload.getAcronym();
        CourseEntity courseEntity = courseDAO.findByAcronym(courseAcronym.toUpperCase());

        if(courseEntity == null) {

            createCourseEntity(courseTransformerPayload);
        } else {

            updateCourseEntity(courseTransformerPayload, courseEntity);
        }
    }

    private void createCourseEntity(CourseTransformerPayload courseTransformerPayload) throws Exception {

        if(courseTransformerPayload.getAcronym() == null) {
            throw new Exception("Could not find acronym");
        }

        CourseEntity courseEntity = new CourseEntity();
        courseEntity.setName(courseTransformerPayload.getName());
        courseEntity.setAcronym(courseTransformerPayload.getAcronym());
        courseEntity.setDescription(courseTransformerPayload.getDescription());
        courseEntity.setCredits(courseTransformerPayload.getCredits());

        addPrerequisites(courseTransformerPayload, courseEntity);

        courseDAO.save(courseEntity);
    }

    private void updateCourseEntity(CourseTransformerPayload courseTransformerPayload, CourseEntity courseEntity) {

        String entityName = courseEntity.getName();
        String payloadName = courseTransformerPayload.getName();
        if(payloadName != null && !payloadName.equals(entityName)) {
            courseEntity.setName(payloadName);
        }

        String entityDescription = courseEntity.getDescription();
        String payloadDescription = courseTransformerPayload.getDescription();
        if(payloadDescription != null && !payloadDescription.equals(entityDescription)) {
            courseEntity.setDescription(payloadDescription);
        }

        Integer entityCredits = courseEntity.getCredits();
        Integer payloadCredits = courseTransformerPayload.getCredits();
        if(payloadCredits != null && !payloadCredits.equals(entityCredits)) {
            courseEntity.setCredits(payloadCredits);
        }

        addPrerequisites(courseTransformerPayload, courseEntity);

        courseDAO.save(courseEntity);
    }

    private void addPrerequisites(CourseTransformerPayload courseTransformerPayload, CourseEntity courseEntity) {

        if (courseTransformerPayload.getPrerequisites() != null &&
                !courseTransformerPayload.getPrerequisites().isEmpty() &&
                courseEntity.getPrerequisites().isEmpty()) {
            List<CoursePrerequisiteEntity> prerequisites = courseTransformerPayload.getPrerequisites().stream()
                    .map(acronym -> acronym.toUpperCase().replace("-", ""))
                    .map(acronym -> courseDAO.findByAcronym(acronym))
                    .filter(courseEntityPrerequisite -> courseEntityPrerequisite != null)
                    .map(prerequisite -> {

                        CoursePrerequisiteEntity coursePrerequisiteEntity = new CoursePrerequisiteEntity();
                        coursePrerequisiteEntity.setCourse(courseEntity);
                        coursePrerequisiteEntity.setPrerequisite(prerequisite);

                        return coursePrerequisiteEntity;
                    })
                    .collect(Collectors.toList());

            courseEntity.setPrerequisites(prerequisites);
        }
    }

}
