package ca.pcuets.core.etl.loader;

public interface Loader<T> {

    void load(T transformerPayload) throws Exception;
}
