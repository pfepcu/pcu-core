package ca.pcuets.core.etl.loader;

import ca.pcuets.core.dao.CourseDAO;
import ca.pcuets.core.dao.SessionCourseDAO;
import ca.pcuets.core.dao.SessionDAO;
import ca.pcuets.core.etl.transformer.payload.CourseSessionTransformerPayload;
import ca.pcuets.core.etl.transformer.payload.SessionScheduleTransformerPayload;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class CourseSessionLoader implements Loader<CourseSessionTransformerPayload> {

    @Resource
    private SessionDAO sessionDAO;

    @Resource
    private CourseDAO courseDAO;

    @Resource
    private SessionCourseDAO sessionCourseDAO;

    @Resource
    private CourseLoader courseLoader;

    @Override
    public void load(CourseSessionTransformerPayload courseSessionTransformerPayload) throws Exception {

        if(courseSessionTransformerPayload.getSessionScheduleTransformerPayloads().isEmpty()) {
            return;
        }

        courseLoader.load(courseSessionTransformerPayload.getCourseTransformerPayload());
        CourseEntity courseEntity = courseDAO.findByAcronym(courseSessionTransformerPayload.getCourseTransformerPayload().getAcronym());

        courseSessionTransformerPayload.getSessionScheduleTransformerPayloads().forEach(sessionScheduleTransformerPayload -> {
            SessionEntity sessionEntity = loadSession(sessionScheduleTransformerPayload);
            loadSessionCourse(sessionScheduleTransformerPayload, courseEntity, sessionEntity);
        });
    }

    private SessionEntity loadSession(SessionScheduleTransformerPayload sessionScheduleTransformerPayload) {

        SeasonEnum season = seasonCharToString(sessionScheduleTransformerPayload.getSession().substring(0,1));
        Integer year = Integer.parseInt("20"+sessionScheduleTransformerPayload.getSession().substring(1));
        SessionEntity sessionEntity = sessionDAO.findBySeasonAndYear(season, year);

        if(sessionEntity == null) {
            sessionEntity = createSessionEntity(season, year);
        }

        return sessionEntity;
    }

    private void loadSessionCourse(SessionScheduleTransformerPayload sessionScheduleTransformerPayload, CourseEntity courseEntity, SessionEntity sessionEntity) {

        SessionCourseEntity sessionCourseEntity = sessionCourseDAO.findByCourseAndSession(courseEntity, sessionEntity);

        if(sessionCourseEntity == null) {
            createSessionCourseEntity(sessionEntity, courseEntity, sessionScheduleTransformerPayload);
        } else {
            updateSessionCourseEntity(sessionScheduleTransformerPayload, sessionCourseEntity);
        }

    }

    private SessionEntity createSessionEntity(SeasonEnum season, Integer year) {

        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setSeason(season);
        sessionEntity.setYear(year);
        sessionEntity = sessionDAO.save(sessionEntity);
        return sessionEntity;
    }

    private void createSessionCourseEntity(SessionEntity sessionEntity, CourseEntity courseEntity, SessionScheduleTransformerPayload sessionScheduleTransformerPayload) {

        SessionCourseEntity sessionCourseEntity = new SessionCourseEntity();
        sessionCourseEntity.setCourse(courseEntity);
        sessionCourseEntity.setSession(sessionEntity);
        sessionCourseEntity.setDay(sessionScheduleTransformerPayload.getDay());
        sessionCourseEntity.setEvening(sessionScheduleTransformerPayload.getEvening());
        sessionCourseDAO.save(sessionCourseEntity);
    }

    private void updateSessionCourseEntity(SessionScheduleTransformerPayload sessionScheduleTransformerPayload, SessionCourseEntity sessionCourseEntity) {

        sessionCourseEntity.setDay(sessionScheduleTransformerPayload.getDay());
        sessionCourseEntity.setEvening(sessionScheduleTransformerPayload.getEvening());
        sessionCourseDAO.save(sessionCourseEntity);
    }

    private SeasonEnum seasonCharToString(String seasonChar) {
        switch (seasonChar) {
            case ("A"):
                return SeasonEnum.AUTUMN;
            case("E"):
                return SeasonEnum.SUMMER;
            case("H"):
                return SeasonEnum.WINTER;
            default:
                return null;
        }
    }
}
