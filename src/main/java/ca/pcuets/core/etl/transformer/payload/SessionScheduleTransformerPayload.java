package ca.pcuets.core.etl.transformer.payload;

public class SessionScheduleTransformerPayload {

    private String session;
    private Boolean isDay;
    private Boolean isEvening;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public Boolean getDay() {
        return isDay;
    }

    public void setDay(Boolean day) {
        isDay = day;
    }

    public Boolean getEvening() {
        return isEvening;
    }

    public void setEvening(Boolean evening) {
        isEvening = evening;
    }
}

