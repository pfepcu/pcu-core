package ca.pcuets.core.etl.transformer.payload;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import java.util.List;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class CourseTransformerPayload {

    private String name;
    private String acronym;
    private String description;
    private Integer credits;
    private List<String> prerequisites;
}
