package ca.pcuets.core.etl.transformer;

public interface Transformer<T, U>{

    T transform(U extractorPayload) throws Exception;
}
