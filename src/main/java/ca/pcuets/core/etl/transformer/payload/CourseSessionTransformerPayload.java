package ca.pcuets.core.etl.transformer.payload;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import java.util.List;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class CourseSessionTransformerPayload {

    private CourseTransformerPayload courseTransformerPayload;
    private List<SessionScheduleTransformerPayload> sessionScheduleTransformerPayloads;
}
