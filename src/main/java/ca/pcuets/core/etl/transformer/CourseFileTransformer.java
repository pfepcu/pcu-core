package ca.pcuets.core.etl.transformer;

import ca.pcuets.core.etl.extractor.payload.HtmlExtractorPayload;
import ca.pcuets.core.etl.transformer.payload.CourseTransformerPayload;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CourseFileTransformer implements Transformer<CourseTransformerPayload, HtmlExtractorPayload> {

    private final String ACRONYM_COURSE_PLAN_SELECTOR = "#plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_HyperLinkPlanCours";
    private final String DESCRIPTION_SELECTOR = "#plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneCenter_FicheCours_LabelDescription";
    private final String CREDITS_SELECTOR = "#plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_LabelCredits";
    private final String PREREQUISITES_SELECTOR = "#plc_lt_zoneMain_pageplaceholder_pageplaceholder_lt_zoneRight_FicheCoursInfosDroite_LabelPrealables a";

    @Override
    public CourseTransformerPayload transform(HtmlExtractorPayload extractorExtractorPayload) throws Exception {

        Document document = extractorExtractorPayload.getDocument();

        CourseTransformerPayload courseTransformerPayload = new CourseTransformerPayload();

        String acronym = findAcronymFromCoursePlan(document);

        if(acronym == null) {
            acronym = findAcronymFromJquery(document);
        }

        courseTransformerPayload.setAcronym(acronym);

        String description = document.select(DESCRIPTION_SELECTOR).text();
        courseTransformerPayload.setDescription(description);

        List<String> prerequisites = findPrerequisites(document);
        courseTransformerPayload.setPrerequisites(prerequisites);

        Integer credits = findCredits(document);
        courseTransformerPayload.setCredits(credits);

        return courseTransformerPayload;
    }

    private String findAcronymFromCoursePlan(Document document) {

        String coursePlanLink = document.select(ACRONYM_COURSE_PLAN_SELECTOR).attr("href");
        Pattern pattern = Pattern.compile("([A-Z]{3}[0-9]{3})");
        Matcher matcher = pattern.matcher(coursePlanLink);

        if(!matcher.find()) {
            return null;
        }

        return matcher.group(1);
    }

    private String findAcronymFromJquery(Document document) {

        String fullDocument = document.toString();
        Pattern pattern = Pattern.compile("\\$\\(\\'#etsTP\\'\\)\\.html\\(\\'([A-Z]+[0-9]+)");
        Matcher matcher = pattern.matcher(fullDocument);

        if(!matcher.find()) {
            return null;
        }

        return matcher.group(1);
    }

    private Integer findCredits(Document document) {

        String credits = document.select(CREDITS_SELECTOR).text();
        Pattern pattern = Pattern.compile("([0-9]+)");
        Matcher matcher = pattern.matcher(credits);

        if(!matcher.find()) {
            return null;
        }

        return Integer.parseInt(matcher.group(1));
    }

    private List<String> findPrerequisites(Document document) {

        String prerequisites = document.select(PREREQUISITES_SELECTOR).text();

        String[] splitPrerequisites = prerequisites.split(" ");

        if(splitPrerequisites.length == 1 && splitPrerequisites[0].equals("")) {
            return Collections.emptyList();
        }

        return Arrays.asList(splitPrerequisites);
    }
}