package ca.pcuets.core.etl.transformer;

import ca.pcuets.core.etl.extractor.payload.XlsExtractorPayload;
import ca.pcuets.core.etl.transformer.payload.CourseSessionTransformerPayload;
import ca.pcuets.core.etl.transformer.payload.CourseTransformerPayload;
import ca.pcuets.core.etl.transformer.payload.SessionScheduleTransformerPayload;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CourseSessionTransformer implements Transformer<List<CourseSessionTransformerPayload>, XlsExtractorPayload> {

    private final String SESSION_PATTERN = "[AaEeHh]([1][7-9]|[2-9][0-9])";
    private final String SCHEDULE_PATTERN = "[J]|[J][S]|[S]";
    private final String ACRONYM_PATTERN = "[A-Z]{3}\\d{3}";
    private final int COLUMN_HEADER_ROW_NUMBER = 1;
    private final int START_YEAR = 17;
    private final String DAY_IDENTIFICATOR = "J";
    private final String EVENING_IDENTIFICATOR = "S";

    @Override
    public List<CourseSessionTransformerPayload> transform(XlsExtractorPayload extractorPayload) throws Exception {
        Sheet sheet = extractorPayload.getWorkBook().getSheetAt(0);
        List<CourseSessionTransformerPayload> courseSessionTransformerPayloadList = Collections.emptyList();
        if(sheet.getPhysicalNumberOfRows() > 0) {
            courseSessionTransformerPayloadList = transformXlsSheet(sheet);
        }
        return courseSessionTransformerPayloadList;
    }

    private List<CourseSessionTransformerPayload> transformXlsSheet(Sheet sheet){
        List<Row> rowList = iteratorToArrayList(sheet.rowIterator());
        List<XlsColumnHeader> columnHeaderList = acquireColumnHeaders(rowList.get(COLUMN_HEADER_ROW_NUMBER));
        List<CourseSessionTransformerPayload> courseSessionTransformerPayloadList = new ArrayList<>();
        for(Row row: rowList){
            if(row.getRowNum()>COLUMN_HEADER_ROW_NUMBER){
                if(row.getPhysicalNumberOfCells() > 0){
                    Cell cell = row.getCell(0);
                    if(cell != null) {
                        if (verifyRegexPattern(ACRONYM_PATTERN, cell.getStringCellValue())) {
                            courseSessionTransformerPayloadList.add(transformXlsRow(row, columnHeaderList.iterator()));
                        }
                    }
                }
            }
        }
        return courseSessionTransformerPayloadList;
    }

    private CourseSessionTransformerPayload transformXlsRow(Row row, Iterator<XlsColumnHeader> xlsColumnHeaderIterator){
        XlsColumnHeader xlsColumnHeader = xlsColumnHeaderIterator.next();
        Iterator<Cell> cellIterator = row.cellIterator();
        Cell cell;

        CourseSessionTransformerPayload courseSessionTransformerPayload = new CourseSessionTransformerPayload();
        CourseTransformerPayload courseTransformerPayload = new CourseTransformerPayload();

        List<SessionScheduleTransformerPayload> sessionSchedulesPayloadList = new ArrayList<>();

        while(cellIterator.hasNext() && xlsColumnHeaderIterator.hasNext()) {
            cell = cellIterator.next();
            int columnIndex = cell.getColumnIndex();
            if(columnIndex == xlsColumnHeader.columnIndex){

                if(columnIndex == 0) {
                    courseTransformerPayload.setAcronym(extractRegexPattern(ACRONYM_PATTERN, cell.getStringCellValue()));
                }else if(columnIndex == 1){
                    courseTransformerPayload.setName(formatCourseName(cell.getStringCellValue()));
                }else {
                    String schedule = cell.getStringCellValue();
                    if(verifyRegexPattern(SCHEDULE_PATTERN, schedule)) {
                        SessionScheduleTransformerPayload sessionSchedule = new SessionScheduleTransformerPayload();
                        sessionSchedule.setDay(schedule.contains(DAY_IDENTIFICATOR));
                        sessionSchedule.setEvening(schedule.contains(EVENING_IDENTIFICATOR));
                        sessionSchedule.setSession(xlsColumnHeader.value);
                        sessionSchedulesPayloadList.add(sessionSchedule);
                    }
                }

                if(xlsColumnHeaderIterator.hasNext()) {
                    xlsColumnHeader = xlsColumnHeaderIterator.next();
                }
            }
        }

        courseSessionTransformerPayload.setSessionScheduleTransformerPayloads(sessionSchedulesPayloadList);
        courseSessionTransformerPayload.setCourseTransformerPayload(courseTransformerPayload);
        return courseSessionTransformerPayload;
    }

    private ArrayList<Row> iteratorToArrayList(Iterator<Row> iterator){
        ArrayList<Row> rows = new ArrayList<>();
        while(iterator.hasNext()){
            rows.add(iterator.next());
        }
        return rows;
    }

    private List<XlsColumnHeader> acquireColumnHeaders(Row row) {
        List<XlsColumnHeader> columnHeaderList = new ArrayList<>();
        Iterator<Cell> cellIterator = row.cellIterator();
        Cell cell;
        if(cellIterator.hasNext()) {
            cell = cellIterator.next();
            columnHeaderList.add(columnHeaderList.size(), acquireColumnHeader(cell));
            if(cellIterator.hasNext()) {
                cell = cellIterator.next();
                columnHeaderList.add(columnHeaderList.size(), acquireColumnHeader(cell));
                while(cellIterator.hasNext()) {
                    cell = cellIterator.next();
                    if(cell.getCellTypeEnum() == CellType.STRING)
                    {
                        String cellValue = cell.getStringCellValue();
                        if(isValidSession(cellValue)) {
                                columnHeaderList.add(columnHeaderList.size(), acquireColumnHeader(cell));
                        }
                    }
                }
            }
        }
        return columnHeaderList;
    }

    private boolean isValidSession(String cellValue) {
        if(verifyRegexPattern(SESSION_PATTERN, cellValue)) {
            if(Integer.parseInt(cellValue.substring(1)) >= START_YEAR) {
                return true;
            }
        }
        return false;
    }

    private XlsColumnHeader acquireColumnHeader(Cell cell) {
        int columnIndex = cell.getColumnIndex();
        String value = cell.getStringCellValue();
        return new XlsColumnHeader(columnIndex, value);
    }

    private boolean verifyRegexPattern(String regex, String valueToVerify) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(valueToVerify);
        return matcher.find();
    }

    private String extractRegexPattern(String regex, String text){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if(matcher.find()){
            return matcher.group(0);
        }
        return "";
    }

    private String formatCourseName(String text){
        return removeLineSeparator(removeTextBetweenParenthesis(text));
    }

    private String removeLineSeparator(String text){
        String lineSeparator1 = "\n";
        String lineSeparator2 = "\r";
        if(text.contains(lineSeparator1)) {
            return text.replaceAll(lineSeparator1, "");
        } else if(text.contains(lineSeparator2)) {
            return text.replaceAll(lineSeparator2, "");
        }
        return text;
    }

    private String removeTextBetweenParenthesis(String text){
        if(text.contains("(")){
            String textWithoutParenthesis = "";
            int firstParenthesisPosition = text.indexOf("(");
            int secondParenthesisPosition = text.indexOf(")");
            textWithoutParenthesis += text.substring(0, firstParenthesisPosition);
            textWithoutParenthesis += text.substring(secondParenthesisPosition+1, text.length());
            return textWithoutParenthesis;
        }
        return text;
    }

    private class XlsColumnHeader{
        int columnIndex;
        String value;
        XlsColumnHeader(int columnIndex, String value){
            this.columnIndex=columnIndex;
            this.value = value;
        }
    }
}