package ca.pcuets.core.etl;

import ca.pcuets.core.dao.CourseDAO;
import ca.pcuets.core.etl.extractor.HtmlExtractor;
import ca.pcuets.core.etl.extractor.XlsExtractor;
import ca.pcuets.core.etl.extractor.payload.HtmlExtractorPayload;
import ca.pcuets.core.etl.extractor.payload.XlsExtractorPayload;
import ca.pcuets.core.etl.loader.CourseLoader;
import ca.pcuets.core.etl.loader.CourseSessionLoader;
import ca.pcuets.core.etl.transformer.CourseFileTransformer;
import ca.pcuets.core.etl.transformer.CourseSessionTransformer;
import ca.pcuets.core.etl.transformer.payload.CourseSessionTransformerPayload;
import ca.pcuets.core.etl.transformer.payload.CourseTransformerPayload;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Component
public class MattDeamon {

    static Logger logger = Logger.getLogger(MattDeamon.class.getName());

    private final List<String> PLANIF_FILES = Arrays.asList(
            "planif/planif_log.xlsx",
            "planif/planif_gti.xls",
            "planif/planif_seg.xls"
    );

    private final String BASE_COURSE_URL = "https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=";

    @Resource
    private HtmlExtractor htmlExtractor;

    @Resource
    private XlsExtractor xlsExtractor;

    @Resource
    private CourseFileTransformer courseFileTransformer;

    @Resource
    private CourseSessionTransformer courseSessionTransformer;

    @Resource
    private CourseLoader courseLoader;

    @Resource
    private CourseSessionLoader courseSessionLoader;

    @Resource
    private CourseDAO courseDAO;

    public void executeImportRoutine() {

        executeCourseSessionImportRoutine();
        executeCourseImportRoutine();

    }

    private void executeCourseSessionImportRoutine() {

        for (String sessionCourseFile: PLANIF_FILES) {

            logger.info(String.format("XSL import of file [%s] starting", sessionCourseFile));

            try {

                XlsExtractorPayload xlsExtractorPayload = xlsExtractor.extract(sessionCourseFile);

                List<CourseSessionTransformerPayload> courseSessionTransformerPayloadList =
                        courseSessionTransformer.transform(xlsExtractorPayload);

                for (CourseSessionTransformerPayload courseSessionTransformerPayload : courseSessionTransformerPayloadList) {

                    courseSessionLoader.load(courseSessionTransformerPayload);
                }

            } catch (Exception e) {

                logger.warning(String.format("XSL import of file [%s] failed during execution: [%s]", sessionCourseFile, e.getMessage()));
            }

            logger.info(String.format("XSL import of file [%s] finished", sessionCourseFile));
        }
    }

    private void executeCourseImportRoutine() {

        courseDAO.findAll().forEach(courseEntity -> {

            if(courseEntity.getAcronym() == null) {
                return;
            }

            String url = getUrlFromAcronym(courseEntity.getAcronym());

            logger.info(String.format("HTML import of url [%s] starting", url));

            try {

                HtmlExtractorPayload htmlExtractorPayload = htmlExtractor.extract(url);
                CourseTransformerPayload courseTransformerPayload = courseFileTransformer.transform(htmlExtractorPayload);
                courseLoader.load(courseTransformerPayload);

            } catch(Exception e) {

                logger.warning(String.format("HTML import of url [%s] failed during execution: [%s]", url, e.getMessage()));
            }

            logger.info(String.format("HTML import of url [%s] finished", url));
        });
    }

    private String getUrlFromAcronym(String acronym) {

        return BASE_COURSE_URL + acronym.toLowerCase();
    }
}
