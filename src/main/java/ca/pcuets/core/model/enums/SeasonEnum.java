package ca.pcuets.core.model.enums;

public enum SeasonEnum {
    WINTER(0),
    SUMMER(1),
    AUTUMN(2);

    private Integer value;

    SeasonEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
