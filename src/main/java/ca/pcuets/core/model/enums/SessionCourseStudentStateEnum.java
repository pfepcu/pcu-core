package ca.pcuets.core.model.enums;

public enum SessionCourseStudentStateEnum {
    PLANNED(0),
    IN_PROGRESS(1),
    PASSED(2),
    FAILED(3);

    private Integer value;

    SessionCourseStudentStateEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}