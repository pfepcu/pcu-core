package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "PROGRAM_REVISION")
@ManagedBean
@Data
@ToString(exclude = {"program", "courseGroups"})
@EqualsAndHashCode
public class ProgramRevisionEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_program_revision")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_program")
    private ProgramEntity program;

    @ManyToOne
    @JoinColumn(name = "id_starting_session", referencedColumnName = "id_session")
    private SessionEntity startingSession;

    @ManyToOne
    @JoinColumn(name = "id_ending_session", referencedColumnName = "id_session")
    private SessionEntity endingSession;

    @ManyToMany
    @JoinTable(name="PROGRAM_REVISION_COURSE_GROUP",
            joinColumns = @JoinColumn(name="id_program_revision"),
            inverseJoinColumns = @JoinColumn(name="id_course_group"))
    private List<CourseGroupEntity> courseGroups;
}
