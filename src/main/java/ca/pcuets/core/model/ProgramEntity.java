package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "PROGRAM")
@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class ProgramEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_program")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "program")
    private List<ConcentrationEntity> concentrationList;

    @OneToMany(mappedBy = "program")
    private List<ProgramRevisionEntity> programRevisions;
}
