package ca.pcuets.core.model;

import ca.pcuets.core.model.enums.SeasonEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SESSION")
@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class SessionEntity implements Comparable<SessionEntity> {

    @Id
    @GeneratedValue
    @Column(name = "id_session")
    private Integer id;

    @Column(name = "season")
    @Enumerated(value = EnumType.ORDINAL)
    private SeasonEnum season;

    @Column(name = "year")
    private Integer year;

    @Override
    public int compareTo(SessionEntity anotherSession) {

        if(this.year.equals(anotherSession.getYear()) && this.season.equals(anotherSession.getSeason()))
            return 0;
        else if((this.year.equals(anotherSession.getYear()) && this.season.getValue() < anotherSession.getSeason().getValue()) || this.year < anotherSession.getYear())
            return -1;
        else
            return 1;
    }
}
