package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SESSION_COURSE")
@ManagedBean
@Data
@ToString(exclude = "course")
@EqualsAndHashCode
public class SessionCourseEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_session_course")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_course", referencedColumnName = "id_course")
    private CourseEntity course;

    @ManyToOne
    @JoinColumn(name = "id_session", referencedColumnName = "id_session")
    private SessionEntity session;

    @Column(name = "day")
    private Boolean day;

    @Column(name = "evening")
    private Boolean evening;
}
