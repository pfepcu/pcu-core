package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COURSE_PREREQUISITE")
@ManagedBean
@Data
@ToString(exclude = {"programRevision"})
@EqualsAndHashCode
public class CoursePrerequisiteEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_course_prerequisite")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_course", referencedColumnName = "id_course")
    private CourseEntity course;

    @ManyToOne
    @JoinColumn(name = "id_prerequisite", referencedColumnName = "id_course")
    private CourseEntity prerequisite;

    @ManyToOne
    @JoinColumn(name = "id_program_revision")
    private ProgramRevisionEntity programRevision;
}
