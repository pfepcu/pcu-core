package ca.pcuets.core.model;

import ca.pcuets.core.model.enums.SessionCourseStudentStateEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SESSION_COURSE_STUDENT")
@ManagedBean
@Data
@ToString(exclude = "user")
@EqualsAndHashCode
public class SessionCourseStudentEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_session_course_student")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "id_session_course", referencedColumnName = "id_session_course")
    private SessionCourseEntity sessionCourse;

    @Column(name = "state")
    @Enumerated(value = EnumType.ORDINAL)
    private SessionCourseStudentStateEnum state;

    @Column(name = "locked")
    private Boolean locked;
}
