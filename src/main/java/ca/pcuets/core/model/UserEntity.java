package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Date;
import java.util.List;

@Entity
@Table (name="USER")
@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class UserEntity {

    @Id
    @GeneratedValue
    @Column(name="id_user")
    private Integer id;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="email", unique=true)
    private String email;

    @Column(name="password")
    private String password;

    @Column(name="creation_time")
    private Date creationTime;

    @Column (name = "success_mattest")
    private Boolean successMattest;

    @Column (name = "success_phytest")
    private Boolean successPhytest;

    @Column (name = "success_inftest")
    private Boolean successInftest;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PreferenceEntity> preferenceEntities;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SessionCourseStudentEntity> sessionCourses;

    @ManyToOne
    @JoinColumn(name = "id_program", referencedColumnName = "id_program")
    private ProgramEntity program;

    @ManyToOne
    @JoinColumn(name = "id_concentration", referencedColumnName = "id_concentration")
    private ConcentrationEntity concentration;

    @ManyToOne
    @JoinColumn(name = "id_starting_session", referencedColumnName = "id_session")
    private SessionEntity startingSession;

    @ManyToMany
    @JoinTable(name="INTEREST_PROFILE",
            joinColumns = @JoinColumn(name="id_user"),
            inverseJoinColumns = @JoinColumn(name="id_course"))
    private List<CourseEntity> interestProfile;

    public void clearSessionCourseStudents() {
        this.sessionCourses.clear();
    }

    public void addSessionCourseStudents(List<SessionCourseStudentEntity> sessionCourseStudents) {
        this.sessionCourses.addAll(sessionCourseStudents);
    }
}
