package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "CONCENTRATION")
@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class ConcentrationEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_concentration")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_program")
    private ProgramEntity program;

    @Column(name = "description")
    private String description;
}
