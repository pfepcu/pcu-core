package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "COURSE_GROUP")
@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class CourseGroupEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_group")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "required_credits")
    private Integer requiredCredits;

    @Column(name = "min")
    private Integer min;

    @Column(name = "max")
    private Integer max;

    @ManyToMany
    @JoinTable(name="PROGRAM_REVISION_COURSE_GROUP",
            joinColumns = @JoinColumn(name="id_course_group"),
            inverseJoinColumns = @JoinColumn(name="id_program_revision"))
    private List<ProgramRevisionEntity> programRevisions;

    @ManyToMany
    @JoinTable(name="COURSE_GROUP_COURSE",
            joinColumns = @JoinColumn(name="id_group"),
            inverseJoinColumns = @JoinColumn(name="id_course"))
    private List<CourseEntity> courses;
}
