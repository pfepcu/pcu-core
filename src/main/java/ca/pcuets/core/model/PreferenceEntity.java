package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name="PREFERENCE")
@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class PreferenceEntity {

    @Id
    @GeneratedValue
    @Column(name="id_preference")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private UserEntity user;

    @Column(name = "eager_internship")
    private Boolean eagerInternship;

    @Column(name = "min_session_course")
    private Integer minSessionCourse;

    @Column(name = "max_session_course")
    private Integer maxSessionCourse;

    @Column(name = "max_evening_course")
    private Integer maxEveningCourse;

    @Column(name = "course_during_internship")
    private Boolean courseDuringInternship;

    @Column(name = "max_course_during_internship")
    private Integer maxCourseDuringInternship;

    @Column(name = "session_between_internship")
    private Integer sessionBetweenInternship;

    @Column(name = "max_session_to_complete")
    private Integer maxSessionToComplete;

}
