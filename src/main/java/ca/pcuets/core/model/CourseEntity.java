package ca.pcuets.core.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "COURSE")
@ManagedBean
@Data
@ToString(exclude = {"courseGroups", "description", "prerequisites"})
@EqualsAndHashCode
public class CourseEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_course")
    private Integer id;

    @Column(name = "acronym", unique = true)
    private String acronym;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "credits")
    private Integer credits;

    @Column(name = "required_credits")
    private Integer requiredCredits;

    @OneToMany(mappedBy = "course")
    private List<SessionCourseEntity> sessionCourses;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    private List<CoursePrerequisiteEntity> prerequisites;

    @ManyToOne
    @JoinColumn(name = "id_successor", referencedColumnName = "id_course")
    private CourseEntity successor;

    @ManyToMany
    @JoinTable(name="COURSE_GROUP_COURSE",
            joinColumns = @JoinColumn(name="id_course"),
            inverseJoinColumns = @JoinColumn(name="id_group"))
    private List<CourseGroupEntity> courseGroups;
}
