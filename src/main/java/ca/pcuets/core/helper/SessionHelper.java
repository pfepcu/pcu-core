package ca.pcuets.core.helper;

import ca.pcuets.core.dao.UserDAO;
import ca.pcuets.core.dto.Credential;
import ca.pcuets.core.model.UserEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class SessionHelper {

    @Resource
    UserDAO userDAO;

    public void createSession(Credential credential, Integer userId) {

        Map<String, Integer> additionalInfos = new HashMap<>();
        additionalInfos.put("userId", userId);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(credential.getEmail(), credential.getPassword(), AuthorityUtils.createAuthorityList("USER"));
        authenticationToken.setDetails(additionalInfos);

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    }

    public void validateSessionUserId(Integer userId) throws IllegalArgumentException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!getSessionUserId().equals(userId) &&
                authentication.getAuthorities().contains(new SimpleGrantedAuthority("USER"))) {

            throw new IllegalArgumentException("Access denied to resource");
        }
    }

    public Integer getSessionUserId() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        try{
            HashMap<String, Integer> additionalInfos = (HashMap<String, Integer>) authentication.getDetails();
            return additionalInfos.get("userId");
        } catch(Exception ignored) {}

        return -1;
    }

    public UserEntity getSessionUser() {
        Optional<UserEntity> userEntityOptional = userDAO.findById(getSessionUserId());

        if(!userEntityOptional.isPresent()) {
            throw new IllegalArgumentException("No user logged in");
        }

        return userEntityOptional.get();
    }
}
