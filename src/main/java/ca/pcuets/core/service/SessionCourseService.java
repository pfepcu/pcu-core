package ca.pcuets.core.service;

import ca.pcuets.core.dao.CourseDAO;
import ca.pcuets.core.dao.SessionCourseDAO;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SessionCourseService {

    @Resource
    private CourseDAO courseDAO;

    @Resource
    private SessionCourseDAO sessionCourseDAO;

    public SessionCourseEntity findByCourseAcronymAndSession(String acronym, SessionEntity sessionEntity) {

        CourseEntity courseEntity = courseDAO.findByAcronym(acronym);
        return sessionCourseDAO.findByCourseAndSession(courseEntity, sessionEntity);
    }

    public SessionCourseEntity findByCourseAndSession(CourseEntity courseEntity, SessionEntity sessionEntity) {

        return sessionCourseDAO.findByCourseAndSession(courseEntity, sessionEntity);
    }
}
