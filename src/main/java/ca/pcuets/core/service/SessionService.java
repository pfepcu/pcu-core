package ca.pcuets.core.service;

import ca.pcuets.core.dao.SessionDAO;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SessionService {

    @Resource
    private SessionDAO sessionDAO;

    public SessionEntity getNextSession(SessionEntity sessionEntity) {

        SeasonEnum season = sessionEntity.getSeason();
        Integer year = sessionEntity.getYear();

        if(season.equals(SeasonEnum.AUTUMN)) {
            return sessionDAO.findBySeasonAndYear(SeasonEnum.WINTER, year + 1);
        } else {
            SeasonEnum newSeason = SeasonEnum.values()[season.ordinal() + 1];
            return sessionDAO.findBySeasonAndYear(newSeason, year);
        }
    }

    public List<SessionEntity> getSessionsFromCourse(CourseEntity selectedCourse) {
        return selectedCourse.getSessionCourses().stream().map(SessionCourseEntity::getSession).collect(Collectors.toList());
    }

    public SessionEntity findByYear(Integer year) {
        return sessionDAO.findByYear(year);
    }

    public SessionEntity findById(Integer id) {

        return sessionDAO.findById(id);
    }
}
