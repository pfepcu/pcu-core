package ca.pcuets.core.service;

import ca.pcuets.core.dao.ProgramRevisionDAO;
import ca.pcuets.core.dto.Course;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.CourseGroupEntity;
import ca.pcuets.core.model.ProgramRevisionEntity;
import ca.pcuets.core.model.enums.SeasonEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProgramRevisionService {

    @Resource
    private ProgramRevisionDAO programRevisionDAO;

    public ProgramRevisionEntity findByStartingYearAndSeason(Integer year, SeasonEnum season) {

        return programRevisionDAO.findRevisionByYearAndSeason(year, season);
    }

    public List<CourseEntity> getInternships(ProgramRevisionEntity programRevisionEntity) {

        return programRevisionEntity.getCourseGroups().stream()
                .map(CourseGroupEntity::getCourses)
                .flatMap(Collection::stream)
                .filter(courseEntity -> courseEntity.getAcronym().startsWith("PC"))
                .collect(Collectors.toList());
    }

    public List<CourseEntity> getCourses(ProgramRevisionEntity programRevisionEntity) {

        return programRevisionEntity.getCourseGroups().stream()
                .map(CourseGroupEntity::getCourses)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }
}
