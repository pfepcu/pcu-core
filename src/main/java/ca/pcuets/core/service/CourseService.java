package ca.pcuets.core.service;

import ca.pcuets.core.dao.CourseDAO;
import ca.pcuets.core.model.CourseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CourseService {

    @Resource
    private CourseDAO courseDAO;

    public CourseEntity findById(Integer id) {

        return courseDAO.findById(id);
    }
}
