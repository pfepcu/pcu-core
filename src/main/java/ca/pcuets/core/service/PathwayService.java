package ca.pcuets.core.service;

import ca.pcuets.core.factory.SessionCourseStudentEntityFactory;
import ca.pcuets.core.model.*;
import ca.pcuets.core.model.enums.SessionCourseStudentStateEnum;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Log
public class PathwayService {

    @Resource
    private ProgramRevisionService programRevisionService;

    @Resource
    private CourseGroupService courseGroupService;

    @Resource
    private SessionService sessionService;

    @Resource
    private SessionCourseService sessionCourseService;

    @Resource
    private SessionCourseStudentService sessionCourseStudentService;

    @Resource
    private SessionCourseStudentEntityFactory sessionCourseStudentEntityFactory;

    @Resource
    private CourseFiltrationService courseFiltrationService;

    public Map<SessionEntity, List<SessionCourseStudentEntity>> generate(UserEntity student,
                                                                         Integer startingSessionId,
                                                                         Integer nextInternshipSessionId) {

        SessionEntity nextInternshipSession = sessionService.findById(nextInternshipSessionId);

        log.info(String.format("Starting generation for student [%s]", student.getEmail()));

        List<SessionCourseStudentEntity> studentSessionCourses = sessionCourseStudentService.findPastSessionCourseStudents(student);
        log.info(String.format("Past student courses [%s]", studentSessionCourses.stream().map(SessionCourseStudentEntity::getSessionCourse).map(SessionCourseEntity::getCourse).map(CourseEntity::getAcronym).collect(Collectors.toList())));

        Map<SessionEntity, List<SessionCourseStudentEntity>> coursesPerSession = new HashMap<>();
        coursesPerSession.put(sessionService.findByYear(0), new ArrayList<>(studentSessionCourses));
        Map<CourseGroupEntity, Integer> courseCountPerGroup = new HashMap<>();
        Integer creditsCount = sessionCourseStudentService.getCreditSumOfSessionCourses(studentSessionCourses);

        SessionEntity currentSession = sessionService.findById(startingSessionId);
        PreferenceEntity studentPreference = student.getPreferenceEntities().get(0);
        ProgramRevisionEntity programRevision = programRevisionService.findByStartingYearAndSeason(currentSession.getYear(), currentSession.getSeason());
        List<CourseEntity> lockedSessionCourseStudents = findLockedCoursesForSession(student.getSessionCourses(), currentSession);
        List<CourseEntity> availableCourses = getAvailableCourses(programRevision, currentSession, studentSessionCourses);
        List<CourseEntity> internships = programRevisionService.getInternships(programRevision);

        while (availableCourses.size() > 0) {

            List<SessionCourseStudentEntity> sessionCourseStudents;

            CourseEntity internship = null;

            if(nextInternshipSession == null ||
                    currentSession.equals(nextInternshipSession) ||
                    currentSession.compareTo(nextInternshipSession) > 0) {
                internship = getInternship(studentPreference, currentSession, nextInternshipSession,
                        studentSessionCourses, creditsCount, internships);
            }
            
            if(internship == null) {
                sessionCourseStudents = generateCourseSession(student, studentPreference, programRevision, currentSession,
                        availableCourses, courseCountPerGroup, lockedSessionCourseStudents);
            } else if (studentPreference.getCourseDuringInternship()) {
                sessionCourseStudents = generateInternshipAndCourseSession(student, studentPreference, programRevision, currentSession, availableCourses, courseCountPerGroup, internship);
            } else {
                sessionCourseStudents = generateInternshipSession(student, internship, currentSession);
            }

            log.info(String.format("Finished generation of session [%s %s] with courses [%s]",
                    currentSession.getSeason().toString(),
                    currentSession.getYear(),
                    sessionCourseStudents.stream().map(SessionCourseStudentEntity::getSessionCourse).map(SessionCourseEntity::getCourse).map(CourseEntity::getAcronym).collect(Collectors.toList())));

            creditsCount += sessionCourseStudentService.getCreditSumOfSessionCourses(sessionCourseStudents);
            studentSessionCourses.addAll(sessionCourseStudents);
            coursesPerSession.put(currentSession, sessionCourseStudents);

            currentSession = sessionService.getNextSession(currentSession);
            lockedSessionCourseStudents = findLockedCoursesForSession(student.getSessionCourses(), currentSession);
            availableCourses = getAvailableCourses(programRevision, currentSession, studentSessionCourses);
        }

        log.info(String.format("Finished generation for student [%s] with a total of [%s] courses and [%s] credits",
                student.getEmail(),
                studentSessionCourses.size(),
                creditsCount));

        return coursesPerSession;
    }

    private List<SessionCourseStudentEntity> generateInternshipAndCourseSession(UserEntity student,
                                                                                PreferenceEntity studentPreference,
                                                                                ProgramRevisionEntity programRevision,
                                                                                SessionEntity currentSession,
                                                                                List<CourseEntity> availableCourses,
                                                                                Map<CourseGroupEntity, Integer> courseCountPerGroup,
                                                                                CourseEntity internship) {
        List<SessionCourseStudentEntity> studentSessionCourses = new ArrayList<>();

        //Add internship to session
        studentSessionCourses.addAll(generateInternshipSession(student, internship, currentSession));

        //Add evening courses to session
        Integer eveningCoursesCount = 0;

        while (eveningCoursesCount < studentPreference.getMaxCourseDuringInternship() && !availableCourses.isEmpty()) {
            CourseEntity selectedCourse = getAndRemoveRandomCourse(availableCourses);
            CourseGroupEntity selectedCourseGroup = courseGroupService.getFromProgramRevisionAndCourse(programRevision, selectedCourse);
            Integer currentCourseGroupCount = getCourseGroupCount(courseCountPerGroup, selectedCourseGroup);
            Boolean isFullGroup = selectedCourseGroup.getMax() != null && currentCourseGroupCount >= selectedCourseGroup.getMax();

            Optional<SessionCourseEntity> sessionCourseEntityOptional = findSessionCourseBySession(selectedCourse.getSessionCourses(), currentSession);

            if (!isFullGroup && sessionCourseEntityOptional.isPresent()) {

                SessionCourseEntity sessionCourseEntity = sessionCourseEntityOptional.get();
                SessionCourseStudentEntity sessionCourseStudentEntity = sessionCourseStudentEntityFactory
                        .get(student, sessionCourseEntity, SessionCourseStudentStateEnum.PLANNED, false);
                if (sessionCourseEntity.getEvening()) {
                    studentSessionCourses.add(sessionCourseStudentEntity);
                    courseCountPerGroup.put(selectedCourseGroup, currentCourseGroupCount + 1);
                    eveningCoursesCount++;
                }
            }
        }
        return studentSessionCourses;
    }

    private List<SessionCourseStudentEntity> generateCourseSession(UserEntity student,
                                                                   PreferenceEntity studentPreference,
                                                                   ProgramRevisionEntity programRevision,
                                                                   SessionEntity currentSession,
                                                                   List<CourseEntity> availableCourses,
                                                                   Map<CourseGroupEntity, Integer> courseCountPerGroup,
                                                                   List<CourseEntity> sessionLockedCourses) {

        Integer dayCoursesCount = 0;
        Integer eveningCoursesCount = 0;
        List<SessionCourseStudentEntity> studentSessionCourses = new ArrayList<>();

        while (dayCoursesCount + eveningCoursesCount < studentPreference.getMaxSessionCourse() && !availableCourses.isEmpty()) {

            Boolean isLockedCourse = false;

            CourseEntity selectedCourse;
            if(!sessionLockedCourses.isEmpty()) {
                selectedCourse = getAndRemoveFirstCourse(sessionLockedCourses);
                availableCourses.remove(selectedCourse);
                isLockedCourse = true;
            } else {
                selectedCourse = getAndRemoveRandomCourse(availableCourses);
            }
            CourseGroupEntity selectedCourseGroup = courseGroupService.getFromProgramRevisionAndCourse(programRevision, selectedCourse);

            if(selectedCourse.getSuccessor() != null && !sessionService.getSessionsFromCourse(selectedCourse).contains(currentSession)) {

                selectedCourse = selectedCourse.getSuccessor();
            }

            Integer currentCourseGroupCount = getCourseGroupCount(courseCountPerGroup, selectedCourseGroup);
            Boolean isFullGroup = selectedCourseGroup.getMax() != null && currentCourseGroupCount >= selectedCourseGroup.getMax();

            Optional<SessionCourseEntity> sessionCourseEntityOptional = findSessionCourseBySession(selectedCourse.getSessionCourses(), currentSession);

            if (!isFullGroup && sessionCourseEntityOptional.isPresent()) {

                SessionCourseEntity sessionCourseEntity = sessionCourseEntityOptional.get();
                SessionCourseStudentEntity sessionCourseStudentEntity = sessionCourseStudentEntityFactory
                        .get(student, sessionCourseEntity, SessionCourseStudentStateEnum.PLANNED, isLockedCourse);

                if (sessionCourseEntity.getDay()) {

                    studentSessionCourses.add(sessionCourseStudentEntity);
                    courseCountPerGroup.put(selectedCourseGroup, currentCourseGroupCount + 1);
                    dayCoursesCount++;
                } else if (sessionCourseEntity.getEvening() && eveningCoursesCount < studentPreference.getMaxEveningCourse()) {

                    studentSessionCourses.add(sessionCourseStudentEntity);
                    courseCountPerGroup.put(selectedCourseGroup, currentCourseGroupCount + 1);
                    eveningCoursesCount++;
                }
            }
        }

        if (student.getStartingSession().equals(currentSession)) {
            studentSessionCourses.add(getPre010SessionCourseStudent(student, currentSession));
        }

        return studentSessionCourses;
    }

    private Optional<SessionCourseEntity> findSessionCourseBySession(List<SessionCourseEntity> sessionCourses,
                                                                     SessionEntity currentSession) {

        return sessionCourses.stream()
                .filter(sessionCourseEntity -> sessionCourseEntity.getSession().equals(currentSession))
                .findFirst();
    }

    private List<CourseEntity> findLockedCoursesForSession(List<SessionCourseStudentEntity> sessionCourseStudents,
                                                           SessionEntity currentSession) {
        return sessionCourseStudents.stream()
                .filter(sessionCourseStudentEntity -> sessionCourseStudentEntity.getSessionCourse().getSession().equals(currentSession))
                .filter(SessionCourseStudentEntity::getLocked)
                .map(SessionCourseStudentEntity::getSessionCourse)
                .map(SessionCourseEntity::getCourse)
                .collect(Collectors.toList());
    }

    private SessionCourseStudentEntity getPre010SessionCourseStudent(UserEntity student,
                                                                     SessionEntity currentSession) {

        SessionCourseEntity sessionCourseEntity = sessionCourseService.findByCourseAcronymAndSession("PRE010", currentSession);
        return sessionCourseStudentEntityFactory.get(student, sessionCourseEntity, SessionCourseStudentStateEnum.PLANNED, false);
    }

    private List<SessionCourseStudentEntity> generateInternshipSession(UserEntity student,
                                                                       CourseEntity internship,
                                                                       SessionEntity currentSession) {

        List<SessionCourseStudentEntity> studentSessionCourses = new ArrayList<>();

        SessionCourseEntity sessionCourseEntity = sessionCourseService.findByCourseAndSession(internship, currentSession);
        SessionCourseStudentEntity sessionCourseStudentEntity = sessionCourseStudentEntityFactory
                .get(student, sessionCourseEntity, SessionCourseStudentStateEnum.PLANNED, false);

        studentSessionCourses.add(sessionCourseStudentEntity);

        return studentSessionCourses;
    }

    private CourseEntity getAndRemoveFirstCourse(List<CourseEntity> availableCourses) {

        CourseEntity selectedCourse = availableCourses.get(0);
        availableCourses.remove(selectedCourse);
        return selectedCourse;
    }

    private CourseEntity getAndRemoveRandomCourse(List<CourseEntity> availableCourses) {

        Random random = new Random();
        CourseEntity selectedCourse = availableCourses.get(random.nextInt(availableCourses.size()));
        availableCourses.remove(selectedCourse);
        return selectedCourse;
    }

    private Integer getCourseGroupCount(Map<CourseGroupEntity, Integer> courseCountPerGroup,
                                        CourseGroupEntity selectedCourseGroup) {

        courseCountPerGroup.putIfAbsent(selectedCourseGroup, 0);
        return courseCountPerGroup.get(selectedCourseGroup);
    }

    private CourseEntity getInternship(PreferenceEntity studentPreference,
                                       SessionEntity currentSession,
                                       SessionEntity nextInternshipSession,
                                       List<SessionCourseStudentEntity> selectedSessionCourses,
                                       Integer creditsCount,
                                       List<CourseEntity> internships) {

        Boolean isNextInternshipSession = currentSession.equals(nextInternshipSession);

        List<SessionCourseEntity> selectedInternships = selectedSessionCourses.stream()
                .map(SessionCourseStudentEntity::getSessionCourse)
                .filter(sessionCourseEntity -> sessionCourseEntity.getCourse().getAcronym().startsWith("PC"))
                .collect(Collectors.toList());

        Integer minSessionBetweenInternship;
        if (studentPreference.getEagerInternship()) {
            minSessionBetweenInternship = 1;
        } else {
            minSessionBetweenInternship = studentPreference.getSessionBetweenInternship();
        }

        SessionCourseEntity internshipDoneWithMinimumSessionRequirement = selectedInternships.stream()
                .filter(sessionCourseEntity -> (currentSession.getId() - sessionCourseEntity.getSession().getId()) > minSessionBetweenInternship)
                .reduce((first, second) -> second).orElse(null);

        CourseEntity selectedInternship = null;
        if (isNextInternshipSession ||
                (creditsCount >= 12 && creditsCount < 46 && selectedInternships.size() == 0) ||
                (creditsCount >= 35 && creditsCount < 80 && selectedInternships.size() == 1 && internshipDoneWithMinimumSessionRequirement != null) ||
                (creditsCount >= 74 && creditsCount < 109 && selectedInternships.size() == 2 && internshipDoneWithMinimumSessionRequirement != null)) {
            selectedInternship = internships.get(selectedInternships.size());
        }
        return selectedInternship;
    }

    private List<CourseEntity> getAvailableCourses(ProgramRevisionEntity studentProgramRevision,
                                                   SessionEntity sessionEntity,
                                                   List<SessionCourseStudentEntity> selectedSessionCourses) {

        List<CourseEntity> availableCourses = studentProgramRevision.getCourseGroups().stream()
                .flatMap(courseGroupEntity -> courseGroupEntity.getCourses().stream())
                .filter(courseEntity -> courseFiltrationService.filterOutCreditLockedCourseGroups(courseEntity, selectedSessionCourses, studentProgramRevision))
                .filter(courseEntity -> courseFiltrationService.filterOutCreditLockedCourses(courseEntity, selectedSessionCourses))
                .filter(courseEntity -> courseFiltrationService.filterOutNotGivenAtSession(courseEntity, sessionEntity))
                .filter(courseEntity -> courseFiltrationService.filterOutPassedCourse(courseEntity, selectedSessionCourses))
                .filter(courseEntity -> courseFiltrationService.filterOutUnfulfilledPrerequisites(courseEntity, selectedSessionCourses, studentProgramRevision))
                .filter(courseEntity -> courseFiltrationService.filterOutMaxedCourseGroups(courseEntity, selectedSessionCourses, studentProgramRevision))
                .filter(courseEntity -> courseFiltrationService.filterOutInternship(courseEntity))
                .collect(Collectors.toList());

        return availableCourses;
    }

}
