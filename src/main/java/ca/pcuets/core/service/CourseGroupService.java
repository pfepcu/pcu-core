package ca.pcuets.core.service;

import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.CourseGroupEntity;
import ca.pcuets.core.model.ProgramRevisionEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseGroupService {

    public CourseGroupEntity getFromProgramRevisionAndCourse(ProgramRevisionEntity programRevision, CourseEntity selectedCourse) {

        return programRevision.getCourseGroups().stream().filter(courseGroupEntity ->
                courseGroupEntity.getCourses().stream().filter(groupCourse ->
                        groupCourse.equals(selectedCourse)).findFirst().isPresent()).findFirst().get();
    }
}
