package ca.pcuets.core.service;

import ca.pcuets.core.dao.SessionCourseDAO;
import ca.pcuets.core.dao.SessionCourseStudentDAO;
import ca.pcuets.core.dao.SessionDAO;
import ca.pcuets.core.factory.SessionCourseStudentEntityFactory;
import ca.pcuets.core.helper.SessionHelper;
import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.UserEntity;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SessionCourseStudentService {

    @Resource
    private SessionCourseStudentDAO sessionCourseStudentDAO;

    @Resource
    private SessionDAO sessionDAO;

    @Resource
    private SessionCourseDAO sessionCourseDAO;

    @Resource
    private SessionCourseStudentEntityFactory sessionCourseStudentEntityFactory;

    @Resource
    private SessionHelper sessionHelper;

    public Integer getCreditSumOfSessionCourses(List<SessionCourseStudentEntity> sessionCourseStudent) {

        return sessionCourseStudent.stream()
                .map(SessionCourseStudentEntity::getSessionCourse)
                .map(SessionCourseEntity::getCourse)
                .map(CourseEntity::getCredits)
                .mapToInt(Integer::intValue)
                .sum();
    }

    public void setLockedCourse(Integer sessionCourseStudentId, Boolean locked){
        SessionCourseStudentEntity sessionCourseStudentEntity = sessionCourseStudentDAO.findById(sessionCourseStudentId);
        sessionCourseStudentEntity.setLocked(locked);
        sessionCourseStudentDAO.save(sessionCourseStudentEntity);
    }

    public void setLockedSession(Integer sessionId, Boolean locked){
        UserEntity user = sessionHelper.getSessionUser();
        List<SessionCourseStudentEntity> coursesOfSession = sessionCourseStudentDAO.findByUserAndSessionCourseSessionId(user, sessionId);
        for (SessionCourseStudentEntity sessionCourseStudentEntity: coursesOfSession) {
            sessionCourseStudentEntity.setLocked(locked);
            sessionCourseStudentDAO.save(sessionCourseStudentEntity);
        }
    }

    public List<SessionCourseStudentEntity> findPastSessionCourseStudents(UserEntity userEntity) {

        SessionEntity sessionEntity = sessionDAO.findByYear(0);
        return sessionCourseStudentDAO.findByUserAndSessionCourseSessionId(userEntity, sessionEntity.getId());
    }

    public SessionCourseStudentEntity findPastSessionCourseStudent(UserEntity userEntity, CourseEntity courseEntity) {

        SessionEntity sessionEntity = sessionDAO.findByYear(0);
        SessionCourseEntity sessionCourseEntity = sessionCourseDAO.findByCourseAndSession(courseEntity, sessionEntity);
        return sessionCourseStudentDAO.findByUserAndSessionCourse(userEntity, sessionCourseEntity);
    }

    public void createPastSessionCourseStudent(UserEntity userEntity, CourseEntity courseEntity) {

        SessionEntity sessionEntity = sessionDAO.findByYear(0);
        SessionCourseEntity sessionCourseEntity = sessionCourseDAO.findByCourseAndSession(courseEntity, sessionEntity);
        SessionCourseStudentEntity sessionCourseStudentEntity = sessionCourseStudentEntityFactory.get(userEntity, sessionCourseEntity, true);
        sessionCourseStudentDAO.save(sessionCourseStudentEntity);
    }

    public void deletePastSessionCourseStudent(UserEntity userEntity, CourseEntity courseEntity) {

        SessionEntity sessionEntity = sessionDAO.findByYear(0);
        SessionCourseEntity sessionCourseEntity = sessionCourseDAO.findByCourseAndSession(courseEntity, sessionEntity);
        SessionCourseStudentEntity sessionCourseStudentEntity = sessionCourseStudentDAO.findByUserAndSessionCourse(userEntity, sessionCourseEntity);
        sessionCourseStudentDAO.delete(sessionCourseStudentEntity);
    }
}
