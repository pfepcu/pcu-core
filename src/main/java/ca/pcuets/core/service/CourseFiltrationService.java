package ca.pcuets.core.service;

import ca.pcuets.core.model.CourseEntity;
import ca.pcuets.core.model.CourseGroupEntity;
import ca.pcuets.core.model.CoursePrerequisiteEntity;
import ca.pcuets.core.model.ProgramRevisionEntity;
import ca.pcuets.core.model.SessionCourseEntity;
import ca.pcuets.core.model.SessionCourseStudentEntity;
import ca.pcuets.core.model.SessionEntity;
import ca.pcuets.core.model.enums.SessionCourseStudentStateEnum;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CourseFiltrationService {

    public boolean filterOutNotGivenAtSession(CourseEntity courseEntity, SessionEntity sessionEntity) {

        List<SessionEntity> sessions = courseEntity.getSessionCourses().stream()
                .map(SessionCourseEntity::getSession)
                .collect(Collectors.toList());

        if(sessions.contains(sessionEntity)) {
            return true;
        } else if(courseEntity.getSuccessor() != null) {
            sessions = courseEntity.getSuccessor().getSessionCourses().stream()
                    .map(SessionCourseEntity::getSession)
                    .collect(Collectors.toList());
        }
        return sessions.contains(sessionEntity);
    }

    public boolean filterOutPassedCourse(CourseEntity courseEntity, List<SessionCourseStudentEntity> selectedSessionCourses) {

        return selectedSessionCourses.stream()
                .filter(sessionCourseStudentEntity -> sessionCourseStudentEntity.getState() != SessionCourseStudentStateEnum.PASSED)
                .map(SessionCourseStudentEntity::getSessionCourse)
                .map(SessionCourseEntity::getCourse)
                .map(CourseEntity::getId)
                .noneMatch(id -> (id.equals(courseEntity.getId()) || id.equals(getSuccessorId(courseEntity))));
    }

    public boolean filterOutUnfulfilledPrerequisites(CourseEntity courseEntity,
                                                     List<SessionCourseStudentEntity> selectedSessionCourses,
                                                     ProgramRevisionEntity studentProgramRevision) {

        Boolean hasUnfulfilledPrerequisite = true;
        Boolean prerequesiteHasSameProgramRevision;

        for (CoursePrerequisiteEntity prerequisite : courseEntity.getPrerequisites()) {
            prerequesiteHasSameProgramRevision = true;
            if (prerequisite.getProgramRevision() != null && !prerequisite.getProgramRevision().getId().equals(studentProgramRevision.getId())) {
                prerequesiteHasSameProgramRevision = false;
            }

            if (prerequesiteHasSameProgramRevision && !getCourseEntityStream(selectedSessionCourses)
                    .collect(Collectors.toList()).contains(prerequisite.getPrerequisite())) {
                hasUnfulfilledPrerequisite = false;
            }
        }

        return hasUnfulfilledPrerequisite;
    }

    public boolean filterOutCreditLockedCourses(CourseEntity courseEntity,
                                                List<SessionCourseStudentEntity> selectedSessionCourses) {

        if(courseEntity.getRequiredCredits() == null) {
            return true;
        }

        Integer creditCount = getCreditCount(selectedSessionCourses);

        return creditCount >= courseEntity.getRequiredCredits();
    }

    public boolean filterOutCreditLockedCourseGroups(CourseEntity courseEntity,
                                                     List<SessionCourseStudentEntity> selectedSessionCourses,
                                                     ProgramRevisionEntity studentProgramRevision) {

        Integer creditCount = getCreditCount(selectedSessionCourses);

        CourseGroupEntity courseGroup = getCourseGroupFromGroupAndProgramRevision(courseEntity, studentProgramRevision);

        if(courseGroup.getRequiredCredits() == null) {
            return true;
        }

        return creditCount >= courseGroup.getRequiredCredits();
    }

    public boolean filterOutMaxedCourseGroups(CourseEntity courseEntity,
                                              List<SessionCourseStudentEntity> selectedSessionCourses,
                                              ProgramRevisionEntity studentProgramRevision) {

        CourseGroupEntity courseGroup = getCourseGroupFromGroupAndProgramRevision(courseEntity, studentProgramRevision);
        List<CourseEntity> sessionCourses = getCourseEntityStream(selectedSessionCourses).collect(Collectors.toList());

        if(courseGroup.getMax() == null) {
            return true;
        }

        Integer maxCourses = courseGroup.getMax();
        Integer groupCount = 0;

        for (CourseEntity groupCourseEntity : courseGroup.getCourses()) {
            if(sessionCourses.contains(groupCourseEntity)) {
                groupCount += 1;
            }
        }

        return groupCount < maxCourses;
    }

    public boolean filterOutInternship(CourseEntity courseEntity) {
        return !courseEntity.getAcronym().contains("PC");
    }

    private Integer getCreditCount(List<SessionCourseStudentEntity> sessionCourses) {

        if(sessionCourses.isEmpty()) {
            return 0;
        }

        return getCourseEntityStream(sessionCourses)
                .map(CourseEntity::getCredits)
                .reduce(Integer::sum)
                .get();
    }

    private CourseGroupEntity getCourseGroupFromGroupAndProgramRevision(CourseEntity courseEntity, ProgramRevisionEntity studentProgramRevision) {

        return studentProgramRevision.getCourseGroups().stream().filter(courseGroupEntity ->
                courseGroupEntity.getCourses().stream().filter(groupCourse ->
                        groupCourse.equals(courseEntity)).findFirst().isPresent()).findFirst().get();
    }

    private Stream<CourseEntity> getCourseEntityStream(List<SessionCourseStudentEntity> selectedSessionCourses) {

        return selectedSessionCourses.stream()
                .map(SessionCourseStudentEntity::getSessionCourse)
                .map(SessionCourseEntity::getCourse);
    }

    private int getSuccessorId(CourseEntity courseEntity) {
        if(courseEntity.getSuccessor() != null){
            return courseEntity.getSuccessor().getId();
        }
        return -1;
    }
}
