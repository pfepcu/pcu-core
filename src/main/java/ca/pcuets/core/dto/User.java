package ca.pcuets.core.dto;

import ca.pcuets.core.dto.validationlevel.CreateValidationLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import javax.annotation.ManagedBean;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class User {

    @NotEmpty(groups = CreateValidationLevel.class)
    private String firstName;

    @NotEmpty(groups = CreateValidationLevel.class)
    private String lastName;

    @NotEmpty(groups = CreateValidationLevel.class)
    @Pattern(regexp = "(([a-z-]+.[a-z]+.[0-9])|[a-z]{2}[0-9]{5})@ens.etsmtl.ca", groups = CreateValidationLevel.class)
    private String email;

    @NotNull(groups = CreateValidationLevel.class)
    private Integer programId;

    @NotNull(groups = CreateValidationLevel.class)
    private Integer concentrationId;

    @NotNull(groups = CreateValidationLevel.class)
    private Boolean successMatTest;

    @NotNull(groups = CreateValidationLevel.class)
    private Boolean successPhyTest;

    @NotNull(groups = CreateValidationLevel.class)
    private Boolean successInfTest;

    @NotNull(groups = CreateValidationLevel.class)
    private Integer startingSessionId;

    @NotEmpty(groups = CreateValidationLevel.class)
    @Size(min = 5, max = 50)
    private String password;
}
