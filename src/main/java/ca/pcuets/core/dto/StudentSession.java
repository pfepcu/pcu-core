package ca.pcuets.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import java.util.List;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class StudentSession implements Comparable<StudentSession> {

    private Integer id;
    private Integer season;
    private Integer year;
    private List<SessionCourse> sessionCourses;

    @Override
    public int compareTo(StudentSession anotherSession) {
        if(this.year.equals(anotherSession.getYear()) && this.season.equals(anotherSession.getSeason()))
            return 0;
        else if((this.year.equals(anotherSession.getYear()) && this.season < anotherSession.getSeason()) || this.year < anotherSession.getYear())
            return -1;
        else
            return 1;
    }
}