package ca.pcuets.core.dto;

import javax.annotation.ManagedBean;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class Session {
    private Integer id;
    private Integer season;
    private Integer year;
}