package ca.pcuets.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class Preference {

    private Integer id;
    private Integer minSessionCourse;
    private Integer maxSessionCourse;
    private Integer maxEveningCourse;
    private Boolean courseDuringInternship;
    private Boolean eagerInternship;
    private Integer maxCourseDuringInternship;
    private Integer sessionBetweenInternship;
    private Integer maxSessionToComplete;
}
