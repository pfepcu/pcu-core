package ca.pcuets.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class Concentration {

    private Integer id;
    private Integer idProgram;
    private String description;
}
