package ca.pcuets.core.dto;

import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class Course {

    private Integer id;
    private String name;
    private String acronym;
    private String description;
    private Integer credits;
    private Integer requiredCredits;
    private List<Course> prerequisites;
    private Course successor;
}
