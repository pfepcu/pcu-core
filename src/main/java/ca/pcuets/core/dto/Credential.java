package ca.pcuets.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import javax.annotation.ManagedBean;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class Credential {

    @NotEmpty
    @Pattern(regexp = "(([a-z-]+.[a-z]+.[0-9])|[a-z]{2}[0-9]{5})@ens.etsmtl.ca")
    private String email;

    @NotEmpty
    @Size(min = 5, max = 50)
    private String password;
}
