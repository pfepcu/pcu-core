package ca.pcuets.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import javax.validation.constraints.NotNull;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class PathwayGenerationRequest {

    @NotNull
    private Integer startingSessionId;

    private Integer nextInternshipSessionId;
}
