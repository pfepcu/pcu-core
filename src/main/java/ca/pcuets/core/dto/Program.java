package ca.pcuets.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class Program {

    private Integer id;
    private String name;
    private String description;
}
