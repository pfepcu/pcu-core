package ca.pcuets.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class SessionCourse {

    private Integer id;
    private Course course;
    private Boolean day;
    private Boolean evening;
    private Boolean locked;
}