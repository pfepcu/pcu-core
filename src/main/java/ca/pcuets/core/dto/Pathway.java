package ca.pcuets.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.ManagedBean;
import java.util.List;

@ManagedBean
@Data
@ToString
@EqualsAndHashCode
public class Pathway {

    private List<StudentSession> studentSessions;
}
