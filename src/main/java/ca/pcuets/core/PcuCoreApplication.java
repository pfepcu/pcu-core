package ca.pcuets.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcuCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(PcuCoreApplication.class, args);
    }
}
