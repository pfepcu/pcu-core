--liquibase formatted sql

CREATE TABLE `PROGRAM` (
  `id_program` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  PRIMARY KEY (`id_program`)
);

CREATE TABLE `PROGRAM_REVISION` (
  `id_program_revision` int(11) NOT NULL AUTO_INCREMENT,
  `id_program` int(11) NOT NULL,
  `id_starting_session` int(11) DEFAULT NULL,
  `id_ending_session` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_program_revision`)
);

CREATE TABLE `COURSE` (
  `id_course` int(11) NOT NULL AUTO_INCREMENT,
  `id_successor` int(11) DEFAULT NULL,
  `acronym` varchar(7) NOT NULL UNIQUE,
  `name` varchar(100) DEFAULT NULL,
  `description` mediumtext,
  `credits` int(11) DEFAULT NULL,
  `required_credits` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_course`)
);

CREATE TABLE `COURSE_GROUP` (
  `id_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `required_credits` int(3) DEFAULT NULL,
  `min` int(11) DEFAULT NULL,
  `max` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_group`)
);

CREATE TABLE `COURSE_GROUP_COURSE` (
  `id_group` int(11) NOT NULL,
  `id_course` int(11) NOT NULL,
  PRIMARY KEY (`id_group`,`id_course`)
);

CREATE TABLE `COURSE_PREREQUISITE` (
  `id_course` int(11) NOT NULL,
  `id_prerequisite` int(11) NOT NULL,
  PRIMARY KEY (`id_course`,`id_prerequisite`)
);

CREATE TABLE `PROGRAM_REVISION_COURSE_GROUP` (
  `id_course_group` int(11) NOT NULL,
  `id_program_revision` int(11) NOT NULL,
  PRIMARY KEY (`id_course_group`, `id_program_revision`)
);

CREATE TABLE `SESSION` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `season` int(4) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_session`)
);

CREATE TABLE `SESSION_COURSE` (
  `id_session_course` int(11) NOT NULL AUTO_INCREMENT,
  `id_course` int(11) NOT NULL,
  `id_session` int(11) NOT NULL,
  `day` bit(1) DEFAULT NULL,
  `evening` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id_session_course`)
);

CREATE TABLE `SESSION_COURSE_STUDENT` (
  `id_session_course_student` int(11) NOT NULL AUTO_INCREMENT,
  `id_session_course` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `state` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_session_course_student`)
);

CREATE TABLE `USER` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_program` int(11) DEFAULT NULL,
  `id_starting_session` int(11) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `creation_time` DATETIME NOT NULL,
  PRIMARY KEY (`id_user`)
);
